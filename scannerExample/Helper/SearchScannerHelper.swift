//
//  SearchScannerHelper.swift
//  scannerExample
//
//  Created by David Chen on 13/07/2018.
//  Copyright © 2018 avision. All rights reserved.
//

import UIKit
import CocoaAsyncSocket

let udpPort = UInt16(55555)
let BROADCAST_PORT = UInt16(5549)
let BROADCAST_TIMEOUT = 5
let SEARCH_SCANNER_TAG = 9191

protocol SearchScannerDelegate: class {
    func didTimeout()
    func broadcastFinished(scannerList:[ScannerModel]?)
}

class SearchScannerHelper: NSObject {
    weak var delegate:SearchScannerDelegate?

    private static var instance:SearchScannerHelper?
    private var udpSocket: GCDAsyncUdpSocket?
    private var timer:Timer?
    private var scannerList:[ScannerModel]?
    
    static func shared() -> SearchScannerHelper {
        if (instance == nil){
            instance=SearchScannerHelper()
        }
        return instance!
    }
    
    override init() {
        super.init()
        udpSocket = GCDAsyncUdpSocket(delegate: self, delegateQueue: DispatchQueue.main)
        startListenUDP()
    }
    
    func startListenUDP(){
        do {
            try udpSocket?.enableBroadcast(true)
        } catch let error as NSError {
            print("fail to enable broadcast: \(error.localizedDescription)")
        }
        
        do {
            try udpSocket?.bind(toPort: udpPort)
        } catch let error as NSError {
            print("fail to bind udp: \(error.localizedDescription)")
        }
        
        do {
            try udpSocket?.beginReceiving()
        } catch let error as NSError {
            print("fail to begin receive: \(error.localizedDescription)")
        }
    }
    
    @objc func timerStop(){
        udpSocket?.close()
        self.delegate?.broadcastFinished(scannerList: scannerList)
    }
    
    public func searchScanner() {
        let bytes:[UInt8] = [
            0x00,/* QR */
            0x02,/* type */
            0x00, 0x00,/* TCP */
            0x00, 0x00, 0x00, 0x00,/* port number */
            0x61, 0x76, 0x32, 0x30, 0x30, 0x30, 0x00, 0x00,/* av2000, */
            0x06, 0x00, 0x00, 0x00, 0x61, 0x76, 0x32, 0x30, 0x30, 0x30, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00];
        let data = Data(bytes: bytes);
        
        self.udpSocket?.send(data, toHost: "255.255.255.255", port: BROADCAST_PORT, withTimeout: -1, tag: SEARCH_SCANNER_TAG)
        self.udpSocket?.send(data, toHost: "224.0.0.245", port: BROADCAST_PORT, withTimeout: -1, tag: SEARCH_SCANNER_TAG)
        
        timer = Timer.scheduledTimer(timeInterval: TimeInterval(BROADCAST_TIMEOUT), target: self, selector: #selector(timerStop), userInfo: nil, repeats: false)
    }
}

extension SearchScannerHelper:GCDAsyncUdpSocketDelegate {
    func udpSocket(_ sock: GCDAsyncUdpSocket, didReceive data: Data, fromAddress address: Data, withFilterContext filterContext: Any?) {
        if data.count > 100 {
            var host: NSString?
            var port: UInt16 = 0
            GCDAsyncUdpSocket.getHost(&host, port: &port, fromAddress: address)
            var scanner = ScannerModel()
            let macAddrData = data.subdata(in: Range(26...32))
            let macAddrDataBytes = [UInt8](macAddrData)
            var macString = ""
            for (index,obj) in macAddrDataBytes.enumerated(){
                if index == macAddrDataBytes.count - 1 {
                    macString.append(String(format:"%02X", obj))
                } else {
                    macString.append(String(format:"%02X:", obj))
                }
            }
            scanner.ipaddr = host! as String
            scanner.macaddr = macString
            scanner.productid = removeNullByte(from: data.subdata(in: Range(68...87)))
            scanner.vendorName = removeNullByte(from: data.subdata(in: Range(56...67)))
            scanner.serialNo = removeNullByte(from: data.subdata(in: Range(88...99)))
            scanner.capability = pad(string: String([UInt8](data)[108], radix: 2), toSize: 8)
            if data.count > 109 {
                scanner.deviceType = removeNullByte(from: data.subdata(in: Range(109...109)))
                scanner.pid = removeNullByte(from: data.subdata(in: Range(110...111)))
                scanner.vid = removeNullByte(from: data.subdata(in: Range(112...113)))
                scanner.scsiPort = removeNullByte(from: data.subdata(in: Range(114...117)))
            }
            if scannerList == nil {
                scannerList = [ScannerModel]()
            }
            scannerList?.append(scanner)
        }
    }
    
    func udpSocket(_ sock: GCDAsyncUdpSocket, didSendDataWithTag tag: Int) {
    }
    
    func udpSocket(_ sock: GCDAsyncUdpSocket, didNotSendDataWithTag tag: Int, dueToError error: Error?) {
    }
    
    func udpSocket(_ sock: GCDAsyncUdpSocket, didNotConnect error: Error?) {
        self.delegate?.didTimeout()
    }
    
    func udpSocketDidClose(_ sock: GCDAsyncUdpSocket, withError error: Error?) {
        
    }
}
