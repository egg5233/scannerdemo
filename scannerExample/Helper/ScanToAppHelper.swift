//
//  BasicScanHelper.swift
//  scannerExample
//
//  Created by David Chen on 16/07/2018.
//  Copyright © 2018 avision. All rights reserved.
//

import Foundation
import CocoaAsyncSocket

let tcpPort = UInt16(55555)
let HEADER_LENGTH = UInt(148)
let rcvHdrTag = 99
let dataTransferTag = 100
let SCANNER_STATUS_IDLE = UInt32(0x0000)
let SCANNER_STATUS_SCANNING = UInt32(0x0001)
let SCANNER_STATUS_ADFPAPEREND = UInt32(0x8004)

@objc protocol ScanToAppHelperProtocol {
    @objc func fileStart(fileSize:NSNumber)
    @objc func fileEnd(filePath:String,page:Int)
}

class ScanToAppHelper:NSObject {
    private static var instance:ScanToAppHelper?
    
    weak var delegate:ScanToAppHelperProtocol?
    
    private var tcpSocket: GCDAsyncSocket?
    private var socketConnected: GCDAsyncSocket?
    private var receivedData:Data?
    
    private var imageProcessQueue:OperationQueue?
    private var pdfProcessQueue:OperationQueue?
    private var lastScanParams:[String:String] =  [String:String]()
    private var avPdfStream:AVISmartImageStream?
    private var avPdfCodec:AVISmartImageCodecs?
    private var pdfData:UnsafeMutablePointer<PDFDATA>?

    private var pdfPath:String?
    private var pdfPathWithOutPage:String?
    private var fileLength:Int?
    private var readBytes:Int?
    private var transferProgress:Int?
    private var file:Data?
    private var jobFolderPath:String?
    private var timeString:String?
    
    private var scanType:Int?
    private var OSAssignPort:Int?
    private var pdfMergedPage:Int?
    private var processingFileNum:Int?
    private var iPage:Int?
    
    static func shared() -> ScanToAppHelper {
        if (instance == nil){
            instance=ScanToAppHelper()
        }
        return instance!
    }
    
    override init() {
        super.init()
        tcpSocket = GCDAsyncSocket(delegate: self, delegateQueue: DispatchQueue.main)
        startListenTCP()
    }
    
    func startListenTCP(){
        do{
            try tcpSocket?.accept(onPort: tcpPort)
        } catch let error as NSError {
            print("fail to bind tcp: \(error.localizedDescription)")
        }
    }
    
    public func scan(at ip:String , with parameter:[String:String]){
        lastScanParams = parameter
        let scanURL = URL(string: "http://" + ip + "/AWIP.html")
        var request = URLRequest(url: scanURL!)
        request.httpMethod="POST"
        var postString = ""
        postString.append("destURL=\(NetworkUtilities.sharedInstance.getMyWiFiAddress()?.ip ?? "")&")
        postString.append("port=\(tcpPort)&")
        postString.append("color=\(parameter[PARAMS_COLOR]!)&")
        postString.append("fileFmt=\(parameter[PARAMS_FORMAT]!)&")
        postString.append("overscan=\(parameter[PARAMS_OVERSCAN]!)&")
        postString.append("predefScanArea=\(parameter[PARAMS_SIZE]!)&")
        postString.append("predefRes=\(parameter[PARAMS_DPI]!)&")
        postString.append("duplex=\(parameter[PARAMS_DUPLEX]!)&")
        postString.append("action=Scan2App&")
        
        request.httpBody = postString.data(using: .utf8)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("error=\(String(describing: error))")
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(String(describing: response))")
            }
            let responseString = String(data: data, encoding: .utf8)
        }
        task.resume()
    }
}

extension ScanToAppHelper:GCDAsyncSocketDelegate {
    func socket(_ sock: GCDAsyncSocket, didAcceptNewSocket newSocket: GCDAsyncSocket) {
        print("didAcceptNew:\(String(describing: newSocket.connectedHost))")
        socketConnected = newSocket
        socketConnected?.readData(toLength: HEADER_LENGTH, withTimeout: -1, tag: rcvHdrTag)
    }
    
    func socket(_ sock: GCDAsyncSocket, didRead data: Data, withTag tag: Int) {
        switch tag {
        case rcvHdrTag:
            var scannerHeader = ScannerHeader()
            scannerHeader.transform(data: data)
            
            switch scannerHeader.jobStatus! {
                //Data transfer
                case 0 :
                    print("data transfer")
                    let fileLength = UInt(CFSwapInt32LittleToHost(data.subdata(in: Range(0...3)).uint32))
                    socketConnected?.readData(toLength: fileLength, withTimeout: -1, tag: dataTransferTag)
                    break
                //Job start
                case 1 :
                    print("job start")
                    iPage = 0;
                    //time
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "YYYY-MM-dd-HH-mm-ss"
                    timeString = dateFormatter.string(from: Date())
                    
                    imageProcessQueue = OperationQueue()
                    imageProcessQueue?.maxConcurrentOperationCount = 1
                    processingFileNum = 0;
                    jobFolderPath = createFolder()
                    socketConnected?.readData(toLength: HEADER_LENGTH, withTimeout: -1, tag: rcvHdrTag)

                    if lastScanParams[PARAMS_FORMAT] == "PDF"{
                        pdfPath = jobFolderPath?.appendingPathComponent("\(timeString!).PDF")
                        pdfPathWithOutPage = jobFolderPath?.appendingPathComponent("\(iPage!)\(timeString!).PDF")

                        pdfProcessQueue = OperationQueue()
                        pdfProcessQueue?.maxConcurrentOperationCount = 1
                        avPdfStream = AVISmartImageStream.init(fileAtPath: pdfPathWithOutPage)
                        avPdfCodec = AVISmartImageCodecs()
                        pdfData = avPdfCodec?.startPDF(with: avPdfStream)
                    }
                    break
                //job end
                case 2 :
                    print("job end")
                    imageProcessQueue?.waitUntilAllOperationsAreFinished()
                    if pdfProcessQueue != nil {
                        pdfProcessQueue?.waitUntilAllOperationsAreFinished()
                    }
                    if lastScanParams[PARAMS_FORMAT] == "PDF" && avPdfCodec != nil{
                        avPdfCodec?.endPDF(pdfData)
                        avPdfCodec = nil
                        avPdfStream = nil
                    }
                    break
                //file start
                case 3 :
                    print("file start")
                    //clear previous info and file
                    readBytes = 0
                    fileLength = 0
                    transferProgress = 0
                    file = Data()
                    socketConnected?.readData(toLength: HEADER_LENGTH, withTimeout: -1, tag: rcvHdrTag)
                    break
                //file end
                case 4 :
                    print("file end")
                    fileLength = readBytes;
                    if iPage == nil {
                        iPage = 0
                    }
                    iPage = iPage! + 1
                    let temp = file
                    
                    var format = ""
                    
                    if (lastScanParams[PARAMS_FORMAT] == "TIFF") {
                        format = "TIF";
                    } else if (lastScanParams[PARAMS_FORMAT] == "JPEG") {
                        format = "JPEG";
                    } else if (lastScanParams[PARAMS_FORMAT] == "PDF") {
                        format = "PDF";
                    }
                    var tmpFile = ScanFileModel()
                    tmpFile.fileName = jobFolderPath?.appendingPathComponent("\(timeString!)_\(iPage!).\(format)")
                    tmpFile.page = iPage;
                    
                    if format != "PDF"{
                        imageProcessQueue?.addOperation {
                            var err:Error?
                            let avCodec = AVISmartImageCodecs()
                            let avStream = AVISmartImageStream.init(fileAtPath: tmpFile.fileName)
                            var avImage:AVISmartImage?
                            do {
                                avImage = try avCodec.load(AVISmartImageStream.init(data: temp))
                            } catch {
                                err = error
                            }
                            
                            //TODO: Auto resize
                            if (self.lastScanParams[PARAMS_FORMAT] == "TIFF") {
                                do {
                                    try avCodec.save(avImage, stream: avStream, format: AVISmartImageFormat.TIFF, colorMode: AVISmartImageColorMode.BW)
                                } catch {
                                    err = error
                                }
                            }
                            
                            if (self.lastScanParams[PARAMS_FORMAT] == "JPEG") {
                                if (self.lastScanParams[PARAMS_COLOR] == "Color") {
                                    do {
                                        try avCodec.save(avImage, stream: avStream, format: AVISmartImageFormat.JPEG, colorMode: AVISmartImageColorMode.color)
                                    } catch {
                                        err = error
                                    }
                                }
                                
                                if (self.lastScanParams[PARAMS_COLOR] == "Gray") {
                                    do {
                                        try avCodec.save(avImage, stream: avStream, format: AVISmartImageFormat.JPEG, colorMode: AVISmartImageColorMode.gray)
                                    } catch {
                                        err = error
                                    }
                                }
                            }
                            self.delegate?.fileEnd(filePath: tmpFile.fileName!, page: tmpFile.page!)
                        }
                    }
                    
                    if format == "PDF"{
                        imageProcessQueue?.addOperation {
                            var err:Error?
                            var avImage:AVISmartImage?
                            do {
                                avImage = try self.avPdfCodec?.load(AVISmartImageStream.init(data: temp))
                            } catch {
                                err = error
                            }
                            
                            if (self.lastScanParams[PARAMS_FORMAT] == "Color") {
                                self.avPdfCodec?.append(avImage, inPDF: self.pdfData, colorMode: AVISmartImageColorMode.color)
                            }
                            
                            if (self.lastScanParams[PARAMS_COLOR] == "Gray") {
                                self.avPdfCodec?.append(avImage, inPDF: self.pdfData, colorMode: AVISmartImageColorMode.gray)
                                
                            }
                            
                            if (self.lastScanParams[PARAMS_COLOR] == "BW") {
                                self.avPdfCodec?.append(avImage, inPDF: self.pdfData, colorMode: AVISmartImageColorMode.BW)
                            }
                        }
                    }
                    
                    socketConnected?.readData(toLength: HEADER_LENGTH, withTimeout: -1, tag: rcvHdrTag)

                    break
                //notification
                case 5 :
                    
                    break
            default:
                break
            }
            
            break
            
        case dataTransferTag:
            file?.append(data)
            readBytes = readBytes!+data.count
            socketConnected?.readData(toLength: HEADER_LENGTH, withTimeout: -1, tag: rcvHdrTag)
            break
        default: break
            
        }
    }
    
    func socketDidDisconnect(_ sock: GCDAsyncSocket, withError err: Error?) {
        socketConnected=nil
        print("disconnect:\(String(describing: err))")
    }
}

