//
//  ScannerCapabilityHelper.swift
//  scannerExample
//
//  Created by David Chen on 17/07/2018.
//  Copyright © 2018 avision. All rights reserved.
//

import Foundation

let PARAMS_DPI           = "Resolution Mode";
let PARAMS_SIZE          = "Paper Size";
let PARAMS_FORMAT        = "File Format";
let PARAMS_DUPLEX        = "Duplex Mode";
let PARAMS_COLOR         = "Color Mode";
let PARAMS_OVERSCAN      = "Over Scan";
