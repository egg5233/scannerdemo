import Foundation

class NetworkUtilities {
    // Get the local ip addresses used by this node
    static let sharedInstance = NetworkUtilities()
    func getIFAddresses() -> [NetInfo] {
        var addresses = [NetInfo]()
        
        // Get list of all interfaces on the local machine:
        var ifaddr : UnsafeMutablePointer<ifaddrs>? = nil
        if getifaddrs(&ifaddr) == 0 {
            
            // For each interface ...
            var ptr = ifaddr
            while ptr != nil {
            ptr = ptr?.pointee.ifa_next
                let flags = Int32((ptr?.pointee.ifa_flags)!)
                var addr = ptr?.pointee.ifa_addr.pointee
                
                // Check for running IPv4, IPv6 interfaces. Skip the loopback interface.
                if (flags & (IFF_UP|IFF_RUNNING|IFF_LOOPBACK)) == (IFF_UP|IFF_RUNNING) {
                    if addr?.sa_family == UInt8(AF_INET) || addr?.sa_family == UInt8(AF_INET6) {
                        
                        // Convert interface address to a human readable string:
                        var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                        if (getnameinfo(&addr!, socklen_t((addr?.sa_len)!), &hostname, socklen_t(hostname.count),
                            nil, socklen_t(0), NI_NUMERICHOST) == 0) {
                                if let address = String(validatingUTF8: hostname) {
                                    
                                    var net = ptr?.pointee.ifa_netmask.pointee
                                    var netmaskName = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                                    getnameinfo(&net!, socklen_t((net?.sa_len)!), &netmaskName, socklen_t(netmaskName.count),
                                        nil, socklen_t(0), NI_NUMERICHOST) == 0
                                    if let netmask = String(validatingUTF8: netmaskName) {
                                        addresses.append(NetInfo(ip: address, netmask: netmask))
                                    }
                                }
                        }
                    }
                }
            }
            freeifaddrs(ifaddr)
        }
        return addresses
    }
    
    // Return IP address of WiFi interface (en0) as a String, or `nil`
    func getMyWiFiAddress() -> NetInfo? {
        var address : String = "error"
        var netmsk : String = "error"
        // Get list of all interfaces on the local machine:
        var ifaddr : UnsafeMutablePointer<ifaddrs>? = nil
        if getifaddrs(&ifaddr) == 0 {
            // For each interface ...
            var ptr = ifaddr
            while ptr != nil {
                ptr = ptr?.pointee.ifa_next
                let interface = ptr?.pointee
                
                // Check for IPv4 or IPv6 interface:
                let addrFamily = interface?.ifa_addr.pointee.sa_family
                if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
                    // Check interface name:
                    if let name = String(validatingUTF8: (interface?.ifa_name)!), name == "en0" {
                        
                        // Convert interface address to a human readable string:
                        var addr = interface?.ifa_addr.pointee
                        var netmask = interface?.ifa_netmask.pointee
                        
                        var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                        var netmaskname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                        
                        getnameinfo(&addr!, socklen_t((interface?.ifa_addr.pointee.sa_len)!),
                            &hostname, socklen_t(hostname.count),
                            nil, socklen_t(0), NI_NUMERICHOST)
                        getnameinfo(&netmask!, socklen_t((interface?.ifa_netmask.pointee.sa_len)!),
                            &netmaskname, socklen_t(netmaskname.count),
                            nil, socklen_t(0), NI_NUMERICHOST)
                        
                        if let address1 = String(validatingUTF8: hostname) {
                            address = address1
                        }
                        if let netmsk1 = String(validatingUTF8: netmaskname) {
                            netmsk = netmsk1
                        }
                    }
                }
            }
            freeifaddrs(ifaddr)
        }
        if address == "error" || netmsk == "error" {
            return nil
        }
        
        return NetInfo(ip: address, netmask: netmsk)
    }
    
}

struct NetInfo {
    // IP Address
    let ip: String
    
    // Netmask Address
    let netmask: String
    
    // CIDR: Classless Inter-Domain Routing
    var cidr: Int {
        var cidr = 0
        for number in binaryRepresentation(netmask) {
            let numberOfOnes = number.components(separatedBy: "1").count - 1
            cidr += numberOfOnes
        }
        return cidr
    }
    
    // Network Address
    var network: String {
        return bitwise(&, net1: ip, net2: netmask)
    }
    
    // Broadcast Address
    var broadcast: String {
        let inverted_netmask = bitwise(~, net1: netmask)
        let broadcast = bitwise(|, net1: network, net2: inverted_netmask)
        return broadcast
    }
    
    fileprivate func binaryRepresentation(_ s: String) -> [String] {
        var result: [String] = []
        
        for numbers in s.components(separatedBy: ".") {
            if let intNumber = Int(numbers) {
                if let binary = Int(String(intNumber, radix: 2)) {
                    result.append(NSString(format: "%08d", binary) as String)
                }
            }
        }
        return result
    }
    
    fileprivate func bitwise(_ op: (UInt8,UInt8) -> UInt8, net1: String, net2: String) -> String {
        let net1numbers = toInts(net1)
        let net2numbers = toInts(net2)
        var result = ""
        for i in 0..<net1numbers.count {
            result += "\(op(net1numbers[i],net2numbers[i]))"
            if i < (net1numbers.count-1) {
                result += "."
            }
        }
        return result
    }
    
    fileprivate func bitwise(_ op: (UInt8) -> UInt8, net1: String) -> String {
        let net1numbers = toInts(net1)
        var result = ""
        for i in 0..<net1numbers.count {
            result += "\(op(net1numbers[i]))"
            if i < (net1numbers.count-1) {
                result += "."
            }
        }
        return result
    }
    
    fileprivate func toInts(_ networkString: String) -> [UInt8] {
        return networkString.components(separatedBy: ".").map{UInt8($0)!}
    }
}
