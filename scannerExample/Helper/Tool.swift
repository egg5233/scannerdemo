//
//  Tool.swift
//  scannerExample
//
//  Created by David on 2018/7/18.
//  Copyright © 2018年 avision. All rights reserved.
//

import Foundation

func removeNullByte(from data:Data) -> String {
    var array = [UInt8](data)
    var returnArray = [UInt8]()
    for i in 0...array.count-1{
        if array[i] == 0 {
            break
        }
        returnArray.append(array[i])
    }
    return String.init(bytes: returnArray, encoding: .ascii) ?? ""
}

func pad(string : String, toSize: Int) -> String {
    var padded = string
    for _ in 0..<(toSize - string.count) {
        padded = "0" + padded
    }
    return padded
}

func createFolder() -> String?{
    let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    let folderPath = documentsDirectory.appendingPathComponent("scan")
    
    let formatter = DateFormatter()
    formatter.dateFormat = "YYYY-MM-dd-HH-mm-ss"
    
    let filePath = folderPath.appendingPathComponent("\(formatter.string(from: Date()))")
    
    do {
        try FileManager.default.createDirectory(atPath: filePath.path, withIntermediateDirectories: true, attributes: nil)
        return filePath.absoluteString
    } catch  {
        return nil
    }
}
