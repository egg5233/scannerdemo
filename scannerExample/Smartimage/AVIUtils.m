//
//  AVIUtils.m
//  Avision-Smart-Image-Demo
//
//  Created by Youwei Teng on 12/04/2017.
//  Copyright © 2017 Youwei Teng. All rights reserved.
//

#import "AVIUtils.h"


@implementation AVIUtils

+ (NSString *)filePathForJPEGNamed:(NSString *)imageNamed {
    return [[NSBundle mainBundle] pathForResource:imageNamed ofType:@"jpeg"];
}

+ (NSString *)filePathForPDFNamed:(NSString *)pdfNamed {
    return [[NSBundle mainBundle] pathForResource:pdfNamed ofType:@"pdf"];
}

+ (NSString *)filePathForTIFFNamed:(NSString *)tiffNamed {
    return [[NSBundle mainBundle] pathForResource:tiffNamed ofType:@"tif"];
}

+ (NSString *)documentFolderPath {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject].path;
}

+ (NSString *)fileInDocumentFolderPath:(NSString *)name extension:(AVIFileExtension)ext {
    NSString *extString = @"";
    
    if (ext == AVIFileExtensionJPEG) {
        extString = @"jpeg";
    } else if (ext == AVIFileExtensionTIFF) {
        extString = @"tif";
    } else if (ext == AVIFileExtensionPDF) {
        extString = @"pdf";
    }
    return [NSString stringWithFormat:@"%@/%@.%@", [AVIUtils documentFolderPath], name, extString];
}
@end
