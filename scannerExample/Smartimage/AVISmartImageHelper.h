//
//  AVISmartImageHelper.h
//  Avision-Smart-Image-Demo
//
//  Created by Youwei Teng on 12/04/2017.
//  Copyright © 2017 Youwei Teng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Ios.h"

@interface AVISmartImageHelper : SmartImageSL

+ (instancetype)shared;

@end
