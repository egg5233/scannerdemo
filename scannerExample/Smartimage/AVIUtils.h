//
//  AVIUtils.h
//  Avision-Smart-Image-Demo
//
//  Created by Youwei Teng on 12/04/2017.
//  Copyright © 2017 Youwei Teng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AVISmartImageDefines.h"

@interface AVIUtils : NSObject

+ (NSString *)filePathForJPEGNamed:(NSString *)imageNamed;
+ (NSString *)filePathForPDFNamed:(NSString *)pdfNamed;
+ (NSString *)filePathForTIFFNamed:(NSString *)tiffNamed;
+ (NSString *)documentFolderPath;
+ (NSString *)fileInDocumentFolderPath:(NSString *)name extension:(AVIFileExtension)ext;

@end
