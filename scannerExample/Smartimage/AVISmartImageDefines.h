//
//  AVISmartImageDefines.h
//  Avision-Smart-Image-Demo
//
//  Created by Youwei Teng on 12/04/2017.
//  Copyright © 2017 Youwei Teng. All rights reserved.
//

#ifndef AVISmartImageDefines_h
#define AVISmartImageDefines_h

typedef NS_ENUM(NSUInteger, AVICodecLoadByteOrder) {
    AVICodecLoadByteOrderRGB  = 0,
    AVICodecLoadByteOrderRGBA = 1,
    AVICodecLoadByteOrderARGB = 2
};

typedef NS_ENUM(NSUInteger, AVISmartImageScanType) {
    AVISmartImageScanTypeFB       = 0,
    AVISmartImageScanTypeADFFront = 1,
    AVISmartImageScanTypeDuplex   = 2,
    AVISmartImageScanTypeADFRear  = 3
};

typedef NS_ENUM(NSUInteger, AVISmartImageFormat) {
    AVISmartImageFormatJPEG = 0,
    AVISmartImageFormatTIFF = 1,
    AVISmartImageFormatPDF  = 2
};

typedef NS_ENUM(NSUInteger, AVIFileExtension) {
    AVIFileExtensionJPEG = 0,
    AVIFileExtensionTIFF,
    AVIFileExtensionPDF
};

typedef NS_ENUM(NSUInteger, AVISmartImageCodecSavePageMode) {
    AVISmartImageCodecSavePageModeAppend,
    AVISmartImageCodecSavePageModeInsert,
    AVISmartImageCodecSavePageModeReplace,
    AVISmartImageCodecSavePageModeOverite
};

typedef NS_ENUM(NSUInteger, AVISmartImageScanMode) {
    //0x01:b/w, 0x02:gray, 0x04:color,
    //0x08:RGBA, 0x10:ARGB,
    AVISmartImageScanModeBW    = 0x01,
    AVISmartImageScanModeGray  = 0x02,
    AVISmartImageScanModeColor = 0x04,
    AVISmartImageScanModeRGBA  = 0x08,
    AVISmartImageScanModeARGB  = 0x10
};

typedef NS_OPTIONS(NSUInteger, AVISmartImageOutMode) {
    AVISmartImageOutModeBW         = (1 << 0),
    AVISmartImageOutModeGray       = (1 << 1),
    AVISmartImageOutModeColor      = (1 << 2),
    AVISmartImageOutModeJPEGAccess = (1 << 8),
    AVISmartImageOutModeAuto       = (1 << 15)
};

typedef NS_OPTIONS(NSUInteger, AVISmartImageColorMode) {
    AVISmartImageColorModeBW         = (1 << 0),
    AVISmartImageColorModeGray       = (1 << 1),
    AVISmartImageColorModeColor      = (1 << 2)
};

typedef NS_ENUM(NSUInteger, AVISmartImageColorChannel) {
    AVISmartImageColorRed   = 0,
    AVISmartImageColorGreen = 1,
    AVISmartImageColorBlue  = 2
};

#endif /* AVISmartImageDefines_h */
