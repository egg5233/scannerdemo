//
//  AVIBWCommand.m
//  Avision-Smart-Image-Demo
//
//  Created by Youwei Teng on 15/04/2017.
//  Copyright © 2017 Youwei Teng. All rights reserved.
//

#import "AVIBWCommand.h"

@implementation AVIBWCommand

- (instancetype)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (int)run:(AVISmartImage *)inputImage error:(NSError *__autoreleasing *)error {
    // pLNIDATA for DeskewCropProc
    long status[1] = {0};
    NIDATA plnidata = {
        .Size = sizeof(NIDATA),
        .Model = "0A230638",
        .InImage = {
            .ScanType = inputImage.scanType, // adf
            .ScanMode = inputImage.scanMode, // color
            .XRes = inputImage.xResolution,
            .YRes = inputImage.yResolution,
            .Pixel = (unsigned int)inputImage.width,
            .Line = (unsigned int)inputImage.height,
            .BitsPerPixel = 24, // grey = 8, color = 24
            .SamplePerPixel = 3,
            .BytesPerLine = (unsigned int)inputImage.bytesPerLine,  // exclude alpha
            .ColorFilter = 0,
            .BackgroundColor = 0,
            .x = 0,
            .y = 0,
            .ScanTypeMaxWidth = inputImage.width + 100, // not sure how many pixel
            .CurPages = 1
        },
        .OutImage = {
            .OutMode = 1, // black and white
            .BW = {
                .Mode = 1,
                .DyThreshold = 0,
                .ColorFilter = 0,
                .BWThreshold = 70, // testing
                .RDropValue  = 70,
                .GDropValue  = 70,
                .BDropValue  = 70,
                .DropThreshold = 50,
                .DropTolerance = 10,
                .BytesPerLine = inputImage.width / 8 // must
            }
        }
    };
    
    unsigned char *buffer = inputImage.rawBuffer;
    unsigned char *rgbBuffer = NULL;
    unsigned char *desBuffer = NULL;
//    if (inputImage.order == AVICodecLoadByteOrderRGBA) {
    // Convert RGBA to RGB buffer
    // Smart Image only support 24 bit
    unsigned long smartImageRGBBufferLength = inputImage.width * inputImage.height * 3;
    unsigned long bytesPerRow = inputImage.width * 4 /* has alpha = 4, no alpha = 3 */;
    // Allocate buffer memeory
    rgbBuffer = malloc(smartImageRGBBufferLength);
    desBuffer = malloc(smartImageRGBBufferLength);
    
    // Skip alpha channel to create 24 bit RGB buffer
    int rgbCount = 0;
    for (int i = 0; i < inputImage.height; i++) {
        for (int j = 0; j < bytesPerRow; j += 4) {
            unsigned long current = i * bytesPerRow + j;
            rgbBuffer[rgbCount] = buffer[current];
            rgbBuffer[rgbCount + 1] = buffer[current + 1];
            rgbBuffer[rgbCount + 2] = buffer[current + 2];
            rgbCount += 3;
        }
    }
//    }      // Run Smart Image process
    [[AVISmartImageHelper new] lThresholdProc:rgbBuffer and:(void **)&desBuffer and:&plnidata and:NULL and:status];
    
    // iOS only support 32 bit RGBA buffer, add alpha channel back
    unsigned long outputBufferLength = inputImage.width * inputImage.height * 4;
    unsigned char *rgbaBuffer = malloc(outputBufferLength);
    unsigned long bytesPerLine = inputImage.width / 8;

    for(int i = 0; i < inputImage.height; i++) {
        for (int j = 0; j < bytesPerLine; j++) {
            unsigned long byteLocation = i * bytesPerLine + j;
            char eightPixels = desBuffer[byteLocation];
            for (int k = 0; k < 8; k++) {
                // k-th bit of eightPixels
                unsigned long base = (byteLocation * 8 + (8 - k)) * 4; // pixel-th
                int value = (eightPixels >> k) & 1 ? 255 : 0;
                // 10110100 >> 3, 10110 & 00000001
                // 10110100 & 10000000
                rgbaBuffer[base]     = value; // R
                rgbaBuffer[base + 1] = value; // G
                rgbaBuffer[base + 2] = value; // B
                rgbaBuffer[base + 3] = (char)255;
            }
        }
    }
    
    NSLog(@"Output Image width = %u, height = %u", plnidata.OutImage.ImageWidth, plnidata.OutImage.ImageHeight);
    NSLog(@"%@ completed, status = %ld", NSStringFromClass([self class]), *status);
    inputImage.data = [NSData dataWithBytes:rgbaBuffer length:outputBufferLength];
    inputImage.width = plnidata.OutImage.ImageWidth;
    inputImage.height = plnidata.OutImage.ImageHeight;
    
    free(rgbBuffer);
    free(desBuffer);
    free(rgbaBuffer);
    
    return YES;
}

@end
