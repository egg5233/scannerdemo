//
//  AVISmartImageCodecs.m
//  Avision-Smart-Image-Demo
//
//  Created by Youwei Teng on 12/04/2017.
//  Copyright © 2017 Youwei Teng. All rights reserved.
//

#import "AVISmartImageCodecs.h"
#import "AVIUtils.h"
#include "tiffio.h"
#import "AVISmartImageConverter.h"

@interface AVISmartImageCodecs ()
@property (nonatomic, assign) unsigned long long totalBytes;
@property (nonatomic, strong) NSFileHandle *fileHandle;
@end

@implementation AVISmartImageCodecs

- (AVISmartImage *)load:(AVISmartImageStream *)stream error:(NSError *__autoreleasing *)error {
    
    UIImage *image = [self imageFromStream:stream];
    CGImageRef imageRef = image.CGImage;
    
    //// Context
    CGContextRef context = NULL;
    CGColorSpaceRef colorSpace;
    unsigned char *bitmapData;
    ////
    
    unsigned int bitsPerPixel = 32; // iOS Devies supports 8 * 4 RGBA
    size_t bitsPerComponent = 8;
    size_t bytesPerPixel = bitsPerPixel / bitsPerComponent;
    
    size_t width = CGImageGetWidth(imageRef);
    size_t height = CGImageGetHeight(imageRef);
    
    size_t bytesPerRow = width * bytesPerPixel;
    size_t bufferLength = bytesPerRow * height;
    
    colorSpace = CGColorSpaceCreateDeviceRGB();
    
    if(!colorSpace) {
        NSLog(@"Error allocating color space RGB\n");
        return NULL;
    }
    
    // Allocate memory for image data
    bitmapData = malloc(bufferLength);
    if(!bitmapData) {
        NSLog(@"Error allocating memory for bitmap\n");
        CGColorSpaceRelease(colorSpace);
        return NULL;
    }
    
    //Create bitmap context
    context = CGBitmapContextCreate(bitmapData,
                                    width,
                                    height,
                                    bitsPerComponent,
                                    bytesPerRow,
                                    colorSpace,
                                    kCGImageAlphaNoneSkipLast);	// RGBX
    if(!context) {
        NSLog(@"Bitmap context not created");
        return nil;
    }
    
    //// Context End
    
    // Draw image into the context to get the raw image data
    CGRect rect = CGRectMake(0, 0, width, height);
    CGContextDrawImage(context, rect, imageRef);
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    AVISmartImage *resultImage = nil;
    
    if(bitmapData) {
        resultImage = [[AVISmartImage alloc] initWithData:[NSData dataWithBytes:bitmapData length:bufferLength] width:width height:height bitsPerPixel:bitsPerPixel order:AVICodecLoadByteOrderRGBA error:error];
        free(bitmapData);
    } else {
        NSLog(@"Error getting bitmap pixel data\n");
    }
    return resultImage;
}

- (AVISmartImage *)load:(AVISmartImageStream *)stream pageNumber:(unsigned long)pageNumber error:(NSError *__autoreleasing *)error {
    return nil;
}

- (AVISmartImage *)load:(AVISmartImageStream *)stream colorMode:(AVISmartImageColorMode)colorMode order:(AVICodecLoadByteOrder)order error:(NSError *__autoreleasing *)error {
    
    return nil;
}

- (AVISmartImage *)load:(AVISmartImageStream *)stream width:(unsigned long)width height:(unsigned long)height colorMode:(AVISmartImageColorMode)colorMode order:(AVICodecLoadByteOrder)order firstPage:(unsigned long)firstPage lastPage:(unsigned long)lastPage error:(NSError *__autoreleasing *)error {
    
    AVISmartImage *image = [[AVISmartImage alloc] initWithData:stream.data width:width height:height bitsPerPixel:32 order:order error:error];
    return image;
}

- (BOOL)save:(AVISmartImage *)image stream:(AVISmartImageStream *)stream format:(AVISmartImageFormat)format colorMode:(AVISmartImageColorMode)colorMode firstPage:(unsigned int)firstPage lastPage:(int)lastPage firstSavePageNumber:(unsigned int)firstSavePageNumber pageMode:(AVISmartImageCodecSavePageMode)pageMode error:(NSError *__autoreleasing *)error {
    return [self save:image stream:stream format:AVISmartImageFormatPDF colorMode:colorMode error:error];
}

- (BOOL)save:(AVISmartImage *)image stream:(AVISmartImageStream *)stream format:(AVISmartImageFormat)format colorMode:(AVISmartImageColorMode)colorMode error:(NSError *__autoreleasing *)error {
    
    switch (format) {
        case AVISmartImageFormatJPEG: {
            if (colorMode == AVISmartImageColorModeGray) {
                return [self saveJPEGImage:image toStream:stream colorMode:AVISmartImageColorModeGray];
            } else if (colorMode == AVISmartImageColorModeColor) {
                return [self saveJPEGImage:image toStream:stream colorMode:AVISmartImageColorModeColor];
            } else {
                return NO;
            }
        }
        case AVISmartImageFormatTIFF: {
            return [self saveTIFFImage:image toStream:stream];
        }
        case AVISmartImageFormatPDF: {
            if (colorMode == AVISmartImageColorModeBW) {
                return [self savePDFImage:image toStream:stream colorMode:AVISmartImageColorModeBW];
            } else if (colorMode == AVISmartImageColorModeGray) {
                return [self savePDFImage:image toStream:stream colorMode:AVISmartImageColorModeGray];
            } else if (colorMode == AVISmartImageColorModeColor) {
                return [self savePDFImage:image toStream:stream colorMode:AVISmartImageColorModeColor];
            } else {
                return NO;
            }
        }
    }
    
    return NO;
}

- (BOOL)saveJPEGImage:(AVISmartImage *)image toStream:(AVISmartImageStream *)stream colorMode:(AVISmartImageColorMode)colorMode {
    NSError *error = nil;
    
    // TODO: implement
    UIImage *imageToSave = nil;
    if (colorMode == AVISmartImageColorModeColor) {
        imageToSave = [AVISmartImageConverter convertToImage:image options:AVISmartImageConvertOptionsNone error:&error];
    } else {
        imageToSave = [AVISmartImageConverter convertToImage:image options:AVISmartImageConvertOptionsGray error:&error];
    }
    NSData *imageData = UIImageJPEGRepresentation(imageToSave, 0.2);
    
//    NSString *jpegPath = [NSString stringWithFormat:@"%@/%@", [AVIUtils documentFolderPath], @"test.jpeg"];
    NSString *jpegPath = stream.filePath;
    if ([imageData writeToFile:jpegPath atomically:YES]) {
        NSLog(@"Successfully write jpeg");
    } else {
        NSLog(@"Failed write jpeg");
    }
    
    return YES;
}

- (BOOL)savePDFImage:(AVISmartImage *)image toStream:(AVISmartImageStream *)stream colorMode:(AVISmartImageColorMode)colorMode {
    // TODO: implement
    unsigned char *outputData = NULL;
    int outputSize[1] = {0};
    unsigned long outputBufferSize = image.data.length;
    unsigned char *outputBuffer = malloc(outputBufferSize);
    unsigned long long totalBytes = 0;
    
//    NSString *pdfPath = [NSString stringWithFormat:@"%@/%@", [AVIUtils documentFolderPath], @"test.pdf"];
    NSString *pdfPath = stream.filePath;
    if ([[NSFileManager defaultManager] createFileAtPath:pdfPath contents:nil attributes:nil]) {
        NSLog(@"Created the File Successfully.");
    } else {
        NSLog(@"Failed to Create the File");
    }
    
    NSFileHandle *fileHandle = [NSFileHandle fileHandleForWritingAtPath:pdfPath];
    
    // ---- Start file ----
    PDFDATA *pdfData = pdf_StartFile(&outputData, outputSize, outputBuffer, outputBufferSize, "");
    totalBytes = [self insertBuffer:outputData bufferSize:*outputSize withFileHandle:fileHandle offset:totalBytes];

    // ---- Start page ----
    if (colorMode == AVISmartImageColorModeColor) {
        pdf_StartPage(pdfData, &outputData, outputSize, AVIPDF_COLORTYPE_COLOR, (int)image.width, (int)image.height, (int)image.xResolution, (int)image.yResolution, AVIPDF_COMPRESSRATE_G31D, AVIPDF_SOURCEFILEFORMAT_RAW);
    } else if (colorMode == AVISmartImageColorModeGray) {
        pdf_StartPage(pdfData, &outputData, outputSize, AVIPDF_COLORTYPE_GRAY, (int)image.width, (int)image.height, (int)image.xResolution, (int)image.yResolution, AVIPDF_COMPRESSRATE_G31D, AVIPDF_SOURCEFILEFORMAT_RAW);
    } else if (colorMode == AVISmartImageColorModeBW) {
        pdf_StartPage(pdfData, &outputData, outputSize, AVIPDF_COLORTYPE_BW, (int)image.width, (int)image.height, (int)image.xResolution, (int)image.yResolution, AVIPDF_COMPRESSRATE_G31D, AVIPDF_SOURCEFILEFORMAT_RAW);
    }
    totalBytes = [self insertBuffer:outputData bufferSize:*outputSize withFileHandle:fileHandle offset:totalBytes];
    
    // ---- Write data ----
    pdf_WriteData(pdfData, &outputData, outputSize, image.rawBuffer, (int)image.data.length);
    totalBytes = [self insertBuffer:outputData bufferSize:*outputSize withFileHandle:fileHandle offset:totalBytes];
    
    // ---- End page ----
    pdf_EndPage(pdfData, &outputData, outputSize);
    totalBytes = [self insertBuffer:outputData bufferSize:*outputSize withFileHandle:fileHandle offset:totalBytes];
    
    // ---- End file ----
    pdf_EndFile(pdfData, &outputData, outputSize);
    totalBytes = [self insertBuffer:outputData bufferSize:*outputSize withFileHandle:fileHandle offset:totalBytes];
    
    NSLog(@"Generated PDF size = %d", pdfData->nOutputFileIndex);
    [fileHandle closeFile];
    
    if (outputData) {
        free(outputData);
    }
    
    if (pdfData) {
        free(pdfData);
    }
    
    return YES;
}

- (unsigned long long)insertBuffer:(unsigned char *)buffer bufferSize:(unsigned long)bufferSize withFileHandle:(NSFileHandle *)fileHandle offset:(unsigned long long)offset {
    NSData *pdfNSData = [NSData dataWithBytes:buffer length:bufferSize];
    [fileHandle seekToFileOffset:offset];
    [fileHandle writeData:pdfNSData];
    NSLog(@"Appended buffer size = %lu", bufferSize);
    
    return bufferSize + offset;
}

- (BOOL)saveTIFFImage:(AVISmartImage *)image toStream:(AVISmartImageStream *)stream {
    NSError *error = nil;
    UIImage *imageToSave = [AVISmartImageConverter convertToImage:image options:AVISmartImageConvertOptionsNone error:&error];
//    NSString *tiffPath = [NSString stringWithFormat:@"%@/%@", [AVIUtils documentFolderPath], @"test.tiff"];
    NSString *tiffPath = stream.filePath;
    [self convertUIImage:imageToSave toTiff:tiffPath withThreshold:120];
    return YES;
}

- (UIImage *)imageFromStream:(AVISmartImageStream *)stream {
    return [UIImage imageWithData:stream.data];
}

- (void)convertUIImage:(UIImage *)uiImage toTiff:(NSString *)file withThreshold:(float)threshold {
    
    CGImageRef srcCGImage = [uiImage CGImage];
    CFDataRef pixelData = CGDataProviderCopyData(CGImageGetDataProvider(srcCGImage));
    unsigned char *pixelDataPtr = (unsigned char *)CFDataGetBytePtr(pixelData);
    
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsDirectory = [paths objectAtIndex:0];
//    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"test.tiff"];
    NSString *filePath = file;
    NSFileManager *manager = [NSFileManager defaultManager];
    if ([manager createFileAtPath:filePath contents:nil attributes:nil]) {
        NSLog(@"Created the File Successfully.");
    } else {
        NSLog(@"Failed to Create the File");
    }
    
    TIFF *tiff;
    if ((tiff = TIFFOpen([file UTF8String], "w")) == NULL) {
        NSLog(@"Unable to write to file.");
        return;
    }
    
    size_t width = CGImageGetWidth(srcCGImage);
    size_t height = CGImageGetHeight(srcCGImage);
    
    TIFFSetField(tiff, TIFFTAG_IMAGEWIDTH, width);
    TIFFSetField(tiff, TIFFTAG_IMAGELENGTH, height);
    TIFFSetField(tiff, TIFFTAG_BITSPERSAMPLE, 1);
    TIFFSetField(tiff, TIFFTAG_SAMPLESPERPIXEL, 1);
    TIFFSetField(tiff, TIFFTAG_ROWSPERSTRIP, 1);
    
    TIFFSetField(tiff, TIFFTAG_COMPRESSION, COMPRESSION_CCITTFAX4);
    TIFFSetField(tiff, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISWHITE);
    TIFFSetField(tiff, TIFFTAG_FILLORDER, FILLORDER_MSB2LSB);
    TIFFSetField(tiff, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
    
    TIFFSetField(tiff, TIFFTAG_XRESOLUTION, 200.0);
    TIFFSetField(tiff, TIFFTAG_YRESOLUTION, 200.0);
    TIFFSetField(tiff, TIFFTAG_RESOLUTIONUNIT, RESUNIT_INCH);
    
    unsigned char *ptr = pixelDataPtr; // initialize pointer to the first byte of the image buffer
    unsigned char red, green, blue, gray, eightPixels;
    tmsize_t bytesPerStrip = ceil(width/8.0);
    unsigned char *strip = (unsigned char *)_TIFFmalloc(bytesPerStrip);
    
    for (int y=0; y<height; y++) {
        for (int x=0; x<width; x++) {
            red = *ptr++; green = *ptr++; blue = *ptr++;
            ptr++; // discard fourth byte by advancing the pointer 1 more byte
            gray = .3 * red + .59 * green + .11 * blue; // http://answers.yahoo.com/question/index?qid=20100608031814AAeBHPU
            eightPixels = strip[x/8];
            eightPixels = eightPixels << 1;
            if (gray < threshold) eightPixels = eightPixels | 1; // black=1 in tiff image without TIFFTAG_PHOTOMETRIC header
            strip[x/8] = eightPixels;
        }
        TIFFWriteEncodedStrip(tiff, y, strip, bytesPerStrip);
    }
    
    TIFFClose(tiff);
    if (strip) _TIFFfree(strip);
    if (pixelData) CFRelease(pixelData);
}

- (PDFDATA *)startPDFWithStream:(AVISmartImageStream *)stream {
    unsigned char *outputData = NULL;
    int outputSize[1] = {0};
//    unsigned long outputBufferSize = image.data.length;
    unsigned long outputBufferSize = 3000 * 3000 * 4; // TODO: read line using buffer
    unsigned char *outputBuffer = malloc(outputBufferSize);
    self.totalBytes = 0;
    
//    NSString *pdfPath = [NSString stringWithFormat:@"%@/%@", [AVIUtils documentFolderPath], @"test.pdf"];
    NSString *pdfPath = stream.filePath;
    if ([[NSFileManager defaultManager] createFileAtPath:pdfPath contents:nil attributes:nil]) {
        NSLog(@"Created the File Successfully.");
    } else {
        NSLog(@"Failed to Create the File");
    }
    
    // ---- Start file ----
    PDFDATA *pdfData = pdf_StartFile(&outputData, outputSize, outputBuffer, outputBufferSize, "");
    pdfData->pdfPath = (char *)[pdfPath UTF8String];
    self.fileHandle = [NSFileHandle fileHandleForWritingAtPath:@(pdfData->pdfPath)];
    self.totalBytes = [self insertBuffer:outputData bufferSize:*outputSize withFileHandle:self.fileHandle offset:0];
    
    return pdfData;
}

- (void)appendImage:(AVISmartImage *)image inPDF:(PDFDATA *)pdfData colorMode:(AVISmartImageColorMode)colorMode {
    unsigned char *outputData = NULL;
    int outputSize[1] = {0};
    
    // ---- Start page ----
    //    pdf_StartPage(pdfData, &outputData, outputSize, AVIPDF_COLORTYPE_COLOR, (int)image.width, (int)image.height, (int)image.xResolution, (int)image.yResolution, AVIPDF_COMPRESSRATE_NONE, AVIPDF_SOURCEFILEFORMAT_RAW);
    if (colorMode == AVISmartImageColorModeColor) {
        pdf_StartPage(pdfData, &outputData, outputSize, AVIPDF_COLORTYPE_COLOR, (int)image.width, (int)image.height, (int)image.xResolution, (int)image.yResolution, AVIPDF_COMPRESSRATE_G31D, AVIPDF_SOURCEFILEFORMAT_RAW);
    } else if (colorMode == AVISmartImageColorModeGray) {
        pdf_StartPage(pdfData, &outputData, outputSize, AVIPDF_COLORTYPE_GRAY, (int)image.width, (int)image.height, (int)image.xResolution, (int)image.yResolution, AVIPDF_COMPRESSRATE_G31D, AVIPDF_SOURCEFILEFORMAT_RAW);
    } else if (colorMode == AVISmartImageColorModeBW) {
        pdf_StartPage(pdfData, &outputData, outputSize, AVIPDF_COLORTYPE_BW, (int)image.width, (int)image.height, (int)image.xResolution, (int)image.yResolution, AVIPDF_COMPRESSRATE_G31D, AVIPDF_SOURCEFILEFORMAT_RAW);
    }
    self.totalBytes = [self insertBuffer:outputData bufferSize:*outputSize withFileHandle:self.fileHandle offset:self.totalBytes];
    
    // ---- Write data ----
    pdf_WriteData(pdfData, &outputData, outputSize, image.rawBuffer, (int)image.data.length);
    self.totalBytes = [self insertBuffer:outputData bufferSize:*outputSize withFileHandle:self.fileHandle offset:self.totalBytes];
    
    // ---- End page ----
    pdf_EndPage(pdfData, &outputData, outputSize);
    self.totalBytes = [self insertBuffer:outputData bufferSize:*outputSize withFileHandle:self.fileHandle offset:self.totalBytes];
}

- (void)endPDF:(PDFDATA *)pdfData {
    unsigned char *outputData = NULL;
    int outputSize[1] = {0};
    
    // ---- End file ----
    pdf_EndFile(pdfData, &outputData, outputSize);
    self.totalBytes = [self insertBuffer:outputData bufferSize:*outputSize withFileHandle:self.fileHandle offset:self.totalBytes];
    
    NSLog(@"Generated PDF size = %d", pdfData->nOutputFileIndex);
    [self.fileHandle closeFile];
    self.totalBytes = 0;
    
    if (pdfData->pOutputBuffer != NULL) {
        free(pdfData->pOutputBuffer);
    }
    
    if (pdfData) {
        free(pdfData);
    }
}

@end
