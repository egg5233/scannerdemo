//
//  AVISmartImageStream.m
//  Avision-Smart-Image-Demo
//
//  Created by Youwei Teng on 12/04/2017.
//  Copyright © 2017 Youwei Teng. All rights reserved.
//

#import "AVISmartImageStream.h"

@interface AVISmartImageStream()

@end

@implementation AVISmartImageStream

- (instancetype)initWithFileAtPath:(NSString *)filePath {
    NSFileHandle *handle = [NSFileHandle fileHandleForReadingAtPath:filePath];
    self = [self initWithData:[handle readDataToEndOfFile]];
    [handle closeFile];
    if (self) {
        self.filePath = filePath;
        self.fileURL = [NSURL fileURLWithPath:filePath];
    }
    return self;
}

- (void)dealloc {
    NSLog(@"AVISmartImageStream dealloc");
}

- (instancetype)initWithData:(NSData *)data {
    self = [super init];
    if (self) {
        self.data = data;
    }
    return self;
}




@end
