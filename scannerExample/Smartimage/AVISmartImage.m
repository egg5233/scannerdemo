//
//  AVISmartImage.m
//  Avision-Smart-Image-Demo
//
//  Created by Youwei Teng on 12/04/2017.
//  Copyright © 2017 Youwei Teng. All rights reserved.
//

#import "AVISmartImage.h"
#import "UIImage+BitmapBuffer.h"

@implementation AVISmartImage

- (instancetype)initWithData:(NSData *)data width:(unsigned long)width height:(unsigned long)height bitsPerPixel:(unsigned int)bitsPerPixel order:(AVICodecLoadByteOrder)order error:(NSError *__autoreleasing *)error {
    self = [super init];
    if (self) {
        self.data = data;
        self.bitsPerPixel = bitsPerPixel; // TODO: Smart Image only support 24
        self.width = width;
        self.height = height;
        self.order = order;
        self.samplePerPixel = 3; // TODO: Smart Image only support 3
        self.scanMode = AVISmartImageScanModeColor;
        self.xResolution = 300;
        self.yResolution = 300;
        self.scanType = AVISmartImageScanTypeADFFront;
        self.bytesPerLine = self.samplePerPixel * width;
    }
    return self;
}

- (instancetype)clone {
    AVISmartImage *clonedImage = [[AVISmartImage alloc] initWithData:[self.data copy] width:self.width height:self.height bitsPerPixel:32 order:AVICodecLoadByteOrderRGBA error:nil];
    return clonedImage;
}

- (void)dealloc {
    self.data = nil;
    NSLog(@"#### AVISmartImage dealloc");
//    if (_rawBuffer) {
//        free(_rawBuffer);
//    }
}

// static methods
+ (instancetype)createColoredWithWidth:(unsigned int)inWidth
                      height:(unsigned int)inHeight
                bitsPerPixel:(unsigned int)inBitsPerPixel
                  resolution:(unsigned int)resolution
             backgroundColor:(AVISmartImageColor *)backgroundColor
                       error:(NSError**)error {
    return nil;
}

- (void)setWidth:(unsigned long)width {
    _width = width;
    _bytesPerLine = width * _samplePerPixel;
}

/**
 Get the ras buffer. 32 bits RGBA

 @return unsigned char
 */
- (unsigned char *)rawBuffer {

    return (unsigned char *)self.data.bytes;
}

- (void)setData:(NSData *)data {
    _data = data;
    _rawBuffer = NULL;
}
@end
