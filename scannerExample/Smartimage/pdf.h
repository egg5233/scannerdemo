#ifndef _PDF_H_
#define _PDF_H_

#define AVIPDF_FALSE                    0
#define AVIPDF_TRUE                     1

#define AVIPDF_COLORTYPE_BW             0
#define AVIPDF_COLORTYPE_GRAY           1
#define AVIPDF_COLORTYPE_COLOR          2

#define AVIPDF_SOURCEFILEFORMAT_RAW     0
#define AVIPDF_SOURCEFILEFORMAT_JPEG    1

#define AVIPDF_COMPRESSRATE_NONE        0
#define AVIPDF_COMPRESSRATE_RUNLENGTH   1
#define AVIPDF_COMPRESSRATE_G31D        2
#define AVIPDF_COMPRESSRATE_G32D        3 // not working
#define AVIPDF_COMPRESSRATE_G4          4 // not working

typedef struct{
    /* +++ Image Setting +++ */
    int nColorType;
    int nImageWidth;
    int nImageHeight;
    int nResX;
    int nResY;
    int nCompressRate;
    /* --- Image Setting --- */
    /* +++ Information +++ */
    char szArtist[64];
    /* --- Information --- */
    /* +++ Work Area +++ */
    int nSourceFileFormat;
    int nPageCount;
    int nObjIndex;
    int nRemainHight;
    int nStripByteCounts;
    /* --- Work Area --- */
    /* +++ Buffer Area +++ */
    unsigned char * pOutputBuffer;
    int nOutputBufferSize;
    int nOutputBufferIndex; /* 在buffer 中目前使用到的位置 */
    int nOutputBufferReadIndex;
    int nOutputFileIndex;
    unsigned char * pUnUseBuffer;
    int nUnUseBufferSize;
    int nUnUseBufferIndex;
    /* +++ Buffer Area +++ */
    char *pdfPath;
}PDFDATA;

PDFDATA *pdf_StartFile(unsigned char ** pOutputData, int * nOutputDataSize, unsigned char * pOutputBuffer, int nOutputBufferSize, char * szCreator);
int pdf_StartPage(PDFDATA * stPdfData, unsigned char ** pOutputData, int * nOutputDataSize, int nColorType, int nImageWidth, int nImageHeight, int nResX, int nResY, int nCompressMethod, int nSourceFileFormat);
int pdf_WriteData(PDFDATA * stPdfData, unsigned char ** pOutputData, int * nOutputDataSize, unsigned char *szImageData, int nImageDataLen);
int pdf_EndPage(PDFDATA * stPdfData, unsigned char ** pOutputData, int * nOutputDataSize);
int pdf_EndFile(PDFDATA * stPdfData, unsigned char ** pOutputData, int * nOutputDataSize);
int pdf_CurFileSize(PDFDATA * stPdfData);
unsigned long long pdf_offsetForPage(int page);
#endif

