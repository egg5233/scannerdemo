#include "stdio.h"
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include "pdf.h"
#import "AVISmartImageConverter.h"
#include "tiffio.h"
#import "AVIUtils.h"
#import "UIImage+BitmapBuffer.h"

//#include "mencode.h"

//#define ENABLE_BW_COMPRESSION

static char pdfHeader[]="%PDF-1.1\r\n";
static char pdfCatalog[]="1 0 obj\r\n<<\r\n/Type /Catalog\r\n/Pages 3 0 R\r\n>>\r\nendobj\r\n";
static char pdfInfo[]="2 0 obj\r\n<<\r\n/Creator(%s)\r\n/CreationDate( %s )\r\n/Author ()\r\n/Producer()\r\n/Title ()\r\n/Subject ()\r\n>>\r\nendobj\r\n";
static char pdfPageTree[]="3 0 obj\r\n<<\r\n/Type /Pages\r\n/Kids [%s]\r\n/Count %d\r\n>>\r\nendobj\r\n";
static char pdfPageTreeB[]="3 0 obj\r\n<<\r\n/Type /Pages\r\n/Kids [";
static char pdfPageTreeE[]="]\r\n/Count %d\r\n>>\r\nendobj\r\n";
static char pdfPage[]="%d 0 obj\r\n<<\r\n/Type /Page\r\n/Parent 3 0 R\r\n/MediaBox [ %d %d %d %d ]\r\n/Rotate 0\r\n/Resources <<\r\n/XObject << /%s %d 0 R >>\r\n/ProcSet [ /PDF /ImageC ]\r\n>>\r\n/Contents %d 0 R\r\n>>\r\nendobj\r\n";
static char pdfPage180[]="%d 0 obj\r\n<<\r\n/Type /Page\r\n/Parent 3 0 R\r\n/MediaBox [ %d %d %d %d ]\r\n/Rotate 180\r\n/Resources <<\r\n/XObject << /%s %d 0 R >>\r\n/ProcSet [ /PDF /ImageC ]\r\n>>\r\n/Contents %d 0 R\r\n>>\r\nendobj\r\n";
static char pdfContents[]="%d 0 obj\r\n<< /Length %d 0 R >>\r\nstream\r\nq\r\n\%d %d %d %d %d %d cm\r\n/%s Do\r\nQ\r\nendstream\r\nendobj\r\n";
static char pdfLength[]="%d 0 obj\r\n%d\r\nendobj\r\n";
static char pdfFilterRunLength[]="RunLengthDecode";
static char pdfFilterG3_1D[]="CCITTFaxDecode /DecodeParms << /EncodedByteAlign true /K 0 /Columns %d >>";
static char pdfFilterG3_2D[]="CCITTFaxDecode /DecodeParms << /EncodedByteAlign true /K %d /Columns %d >>";
static char pdfFilterG4_2D[]="CCITTFaxDecode /DecodeParms << /K -1 /Columns %d >>";
static char pdfXObjectB[]="%d 0 obj\r\n<<\r\n/Type /XObject\r\n/Subtype /Image\r\n/Name /%s\r\n/Filter /%s\r\n /BlackIs1 %s\r\n /Width %d\r\n/Height %d\r\n/BitsPerComponent 1\r\n/ColorSpace /DeviceGray\r\n/Length %d 0 R\r\n>>\r\nstream\r\n"; 
static char pdfXObjectBNone[]="%d 0 obj\r\n<<\r\n/Type /XObject\r\n/Subtype /Image\r\n/Name /%s\r\n /BlackIs1 %s\r\n /Width %d\r\n/Height %d\r\n/BitsPerComponent 1\r\n/ColorSpace /DeviceGray\r\n/Length %d 0 R\r\n>>\r\nstream\r\n"; 
static char pdfXObjectG[]="%d 0 obj\r\n<<\r\n/Type /XObject\r\n/Subtype /Image\r\n/Name /%s\r\n/Filter /DCTDecode\r\n/Width %d\r\n/Height %d\r\n/BitsPerComponent 8\r\n/ColorSpace /DeviceGray\r\n/Length %d 0 R\r\n>>\r\nstream\r\n";
static char pdfXObjectC[]="%d 0 obj\r\n<<\r\n/Type /XObject\r\n/Subtype /Image\r\n/Name /%s\r\n/Filter /DCTDecode\r\n/Width %d\r\n/Height %d\r\n/BitsPerComponent 8\r\n/ColorSpace /DeviceRGB\r\n/Length %d 0 R\r\n>>\r\nstream\r\n";
static char pdfXObjectE[]="\r\nendstream\r\nendobj\r\n";
static char pdfXrefTable[]="xref\r\n0 %d \r\n";
static char pdfTrailer[]="trailer\r\n<<\r\n/Size %d\r\n/Root 1 0 R\r\n/Info 2 0 R\r\n>>\r\nstartxref\r\n%d\r\n%%%%EOF\r\n";

static int nOutputIndex;

typedef struct {
    int offset;
    int generation;
    char type;
} XREFTABLE;
#define PDF_MAX_PAGES   999 
static XREFTABLE XrefTable[4 + PDF_MAX_PAGES * 5];
static char szPageTree[PDF_MAX_PAGES*10+1];
/* for G3 compression */
static int nFilter=2, K=2;
#define HDR_BUF_SIZE 1024
char PdfHeaderBuf[HDR_BUF_SIZE];
//char * gpRefTmp = NULL;
unsigned char gRefLine[640];

int pdf_outputData(PDFDATA * stPdfData, unsigned char * pData, unsigned long long nDataSize)
{
    int nRet = AVIPDF_FALSE;
    
    if(stPdfData == NULL || pData == NULL)
        return nRet;
    if(stPdfData->nOutputBufferIndex + nDataSize <= stPdfData->nOutputBufferSize)
    {
        memcpy(&stPdfData->pOutputBuffer[stPdfData->nOutputBufferIndex], pData, nDataSize);
        stPdfData->nOutputBufferIndex = stPdfData->nOutputBufferIndex + nDataSize;
        stPdfData->nOutputFileIndex = stPdfData->nOutputFileIndex + nDataSize;
        nRet = AVIPDF_TRUE;
    }

    return nRet;
}

PDFDATA * pdf_StartFile(unsigned char ** pOutputData, int * nOutputDataSize, unsigned char * pOutputBuffer, int nOutputBufferSize, char * szCreator)
{
    PDFDATA * stPdfData = NULL;

    *pOutputData = NULL;
    unsigned long pdfDataLength = sizeof(PDFDATA);
    stPdfData = malloc(pdfDataLength);
    if(stPdfData == NULL)
        return NULL;

    memset(stPdfData, 0x00, sizeof(PDFDATA));
    stPdfData->pOutputBuffer = pOutputBuffer;
    stPdfData->nOutputBufferSize = nOutputBufferSize;
    stPdfData->nOutputFileIndex = 0;
    stPdfData->nOutputBufferReadIndex = 0;
    if(szCreator != NULL)
    {
        if(strlen(szCreator) <= 0 || strlen(szCreator) >= 64)
            sprintf(stPdfData->szArtist, "%s", "Avision");
        else
            sprintf(stPdfData->szArtist, "%s", szCreator);
    }
    /* header */
    stPdfData->nObjIndex = 0;
    stPdfData->nPageCount = 0;

    XrefTable[stPdfData->nObjIndex].offset = 0;
    XrefTable[stPdfData->nObjIndex].generation = 65535;
    XrefTable[stPdfData->nObjIndex].type = 'f';    
    pdf_outputData(stPdfData, (unsigned char *)pdfHeader, sizeof(pdfHeader));

    stPdfData->nObjIndex += 3;

    if(stPdfData->nOutputBufferIndex > stPdfData->nOutputBufferReadIndex)
    {
        *pOutputData = &stPdfData->pOutputBuffer[stPdfData->nOutputBufferReadIndex];
        *nOutputDataSize = stPdfData->nOutputBufferIndex - stPdfData->nOutputBufferReadIndex;
        stPdfData->nOutputBufferReadIndex = stPdfData->nOutputBufferIndex;
    }
    else
    {
        *pOutputData = NULL;
        *nOutputDataSize = 0;
    }
    return stPdfData;
}

int pdf_StartPage(PDFDATA * stPdfData, unsigned char ** pOutputData, int * nOutputDataSize, int nColorType, int nImageWidth, int nImageHeight, int nResX, int nResY, int nCompressMethod, int nSourceFileFormat)
{
    int nRet = AVIPDF_FALSE;
    char buf[512], szPageString[10], *ptr;
    char szFilter[256];


    *pOutputData = NULL;
    
    if(stPdfData == NULL)
        return nRet;

    stPdfData->nColorType = nColorType;
    stPdfData->nImageWidth = nImageWidth;
    stPdfData->nImageHeight = nImageHeight;
    stPdfData->nResX = nResX;
    stPdfData->nResY = nResY;
    stPdfData->nRemainHight = nImageHeight;
    stPdfData->nStripByteCounts = 0;
    stPdfData->nCompressRate = nCompressMethod;
    stPdfData->nUnUseBufferSize = stPdfData->nImageWidth * 3 * 3;
    stPdfData->pUnUseBuffer = malloc(stPdfData->nUnUseBufferSize);
    stPdfData->nUnUseBufferIndex = 0;
    stPdfData->nSourceFileFormat = nSourceFileFormat;

    stPdfData->nObjIndex++;
    stPdfData->nPageCount++;
    sprintf(szPageString, "I%d", stPdfData->nPageCount);

    /* �NpdfPage�[��PdfHeaderBuf */
    XrefTable[stPdfData->nObjIndex].offset = stPdfData->nOutputFileIndex;
    XrefTable[stPdfData->nObjIndex].generation = 0;
    XrefTable[stPdfData->nObjIndex].type = 'n';
    sprintf(buf, pdfPage, stPdfData->nObjIndex, 0, 0, stPdfData->nImageWidth*72/stPdfData->nResX, stPdfData->nImageHeight*72/stPdfData->nResY, szPageString, stPdfData->nObjIndex+1, stPdfData->nObjIndex+2);
    pdf_outputData(stPdfData, buf, strlen(buf));

    XrefTable[stPdfData->nObjIndex+2].offset = stPdfData->nOutputFileIndex;
    XrefTable[stPdfData->nObjIndex+2].generation = 0;
    XrefTable[stPdfData->nObjIndex+2].type = 'n';
    sprintf(buf, pdfContents, stPdfData->nObjIndex+2, stPdfData->nObjIndex+3, stPdfData->nImageWidth*72/stPdfData->nResX, 0, 0, stPdfData->nImageHeight*72/stPdfData->nResY, 0, 0, szPageString);
    pdf_outputData(stPdfData, buf, strlen(buf));

    XrefTable[stPdfData->nObjIndex+3].offset = stPdfData->nOutputFileIndex;
    XrefTable[stPdfData->nObjIndex+3].generation = 0;
    XrefTable[stPdfData->nObjIndex+3].type = 'n';
    ptr = strstr(buf, "endstream");
    if(ptr == NULL) return 0;
    *ptr = '\0';
    ptr = strchr(buf, 'q');
    if(ptr == NULL) return 0;
    sprintf(buf, pdfLength, stPdfData->nObjIndex+3, strlen(ptr));
    pdf_outputData(stPdfData, buf, strlen(buf));

    XrefTable[stPdfData->nObjIndex+1].offset = stPdfData->nOutputFileIndex;
    XrefTable[stPdfData->nObjIndex+1].generation = 0;
    XrefTable[stPdfData->nObjIndex+1].type = 'n';
    if(stPdfData->nColorType == AVIPDF_COLORTYPE_BW)
    {
        if(stPdfData->nCompressRate == AVIPDF_COMPRESSRATE_NONE)
        {
            sprintf(buf, pdfXObjectBNone, stPdfData->nObjIndex+1, szPageString, "false", stPdfData->nImageWidth, stPdfData->nImageHeight, stPdfData->nObjIndex+4);
        }
        else if(stPdfData->nCompressRate == AVIPDF_COMPRESSRATE_RUNLENGTH)
        {
            sprintf(buf, pdfXObjectB, stPdfData->nObjIndex+1, szPageString, pdfFilterRunLength, "false", stPdfData->nImageWidth, stPdfData->nImageHeight, stPdfData->nObjIndex+4);
        }
        else if(stPdfData->nCompressRate == AVIPDF_COMPRESSRATE_G31D)
        {
            sprintf(szFilter, pdfFilterG3_1D, stPdfData->nImageWidth);
            sprintf(buf, pdfXObjectB, stPdfData->nObjIndex+1, szPageString, szFilter, "true", stPdfData->nImageWidth, stPdfData->nImageHeight, stPdfData->nObjIndex+4);
        }
        else if(stPdfData->nCompressRate == AVIPDF_COMPRESSRATE_G32D)
        {
            sprintf(szFilter, pdfFilterG3_2D, K, stPdfData->nImageWidth);
            sprintf(buf, pdfXObjectB, stPdfData->nObjIndex+1, szPageString, szFilter, "true", stPdfData->nImageWidth, stPdfData->nImageHeight, stPdfData->nObjIndex+4);
        }
        else if(stPdfData->nCompressRate == AVIPDF_COMPRESSRATE_G4)
        {
#if 0
            if(gpRefTmp != NULL)
            {
                free(gpRefTmp);
                gpRefTmp = NULL;
            }
#endif
            memset(gRefLine, 0xFF, 640);
#ifdef ENABLE_BW_COMPRESSION
            G3Encode_MMRF_Init();
#endif
            sprintf(szFilter, pdfFilterG4_2D, stPdfData->nImageWidth);
            sprintf(buf, pdfXObjectB, stPdfData->nObjIndex+1, szPageString, szFilter, "true", stPdfData->nImageWidth, stPdfData->nImageHeight, stPdfData->nObjIndex+4);
        }
        pdf_outputData(stPdfData, buf, strlen(buf));
    }
    else if(stPdfData->nColorType == AVIPDF_COLORTYPE_GRAY || stPdfData->nColorType == AVIPDF_COLORTYPE_COLOR)
    {
        if(stPdfData->nColorType == AVIPDF_COLORTYPE_GRAY)
            sprintf(buf, pdfXObjectG, stPdfData->nObjIndex+1, szPageString, stPdfData->nImageWidth, stPdfData->nImageHeight, stPdfData->nObjIndex+4);
        else
            sprintf(buf, pdfXObjectC, stPdfData->nObjIndex+1, szPageString, stPdfData->nImageWidth, stPdfData->nImageHeight, stPdfData->nObjIndex+4);

        pdf_outputData(stPdfData, buf, strlen(buf));
        if(stPdfData->nSourceFileFormat == AVIPDF_SOURCEFILEFORMAT_RAW)
        {
//            stPdfData->sJpegData =
            //stPdfData->sJpegData = JPEGStartFileB(&stPdfData->pBuff[stPdfData->nBufferCurIndex], stPdfData->nBuffSize - stPdfData->nBufferCurIndex);
            //JPEGStartPage(stPdfData->sJpegData, stPdfData->nImgWidth, stPdfData->nImgHeight, stPdfData->nColorType, 100);
        }
    }

    if(stPdfData->nOutputBufferIndex > stPdfData->nOutputBufferReadIndex)
    {
        *pOutputData = &stPdfData->pOutputBuffer[stPdfData->nOutputBufferReadIndex];
        *nOutputDataSize = stPdfData->nOutputBufferIndex - stPdfData->nOutputBufferReadIndex;
        stPdfData->nOutputBufferReadIndex = stPdfData->nOutputBufferIndex;
    }
    else
    {
        *pOutputData = NULL;
        *nOutputDataSize = 0;
    }

    nRet = AVIPDF_TRUE;
    return nRet;
}

int pdf_WriteDataLine(PDFDATA * stPdfData, unsigned char *szImageData, int nImageDataLen)
{
    int nRet = AVIPDF_FALSE;
    int nImgWidthInBytes = 0, lines = 0;
    unsigned char * pDataCompressed = NULL;
    
    if(stPdfData == NULL)
        return nRet;
    
    if(stPdfData->nColorType == AVIPDF_COLORTYPE_BW)
    {
        nImgWidthInBytes = (stPdfData->nImageWidth + 7) / 8;
        lines = nImageDataLen / nImgWidthInBytes;
        pDataCompressed = malloc(nImgWidthInBytes * 10);
    }
    if(stPdfData->nColorType == AVIPDF_COLORTYPE_BW)
    {
        if(stPdfData->nCompressRate == AVIPDF_COMPRESSRATE_G31D || stPdfData->nCompressRate == AVIPDF_COMPRESSRATE_RUNLENGTH)
        {
            NSString *tiffPath = [NSString stringWithFormat:@"%@/%@", [AVIUtils documentFolderPath], @"cpdftmp.tiff"];
            // TODO: pass threshold in
            int threshold = 120;
            NSError *error = nil;
            
            UIImage *uiImage = [AVISmartImageConverter convertToImage:szImageData width:stPdfData->nImageWidth height:stPdfData->nImageHeight options:AVISmartImageConvertOptionsNone error:&error];
            CGImageRef srcCGImage = [uiImage CGImage];
            CFDataRef pixelData = CGDataProviderCopyData(CGImageGetDataProvider(srcCGImage));
            unsigned char *pixelDataPtr = (unsigned char *)CFDataGetBytePtr(pixelData);
            
            TIFF *tiff;
            if ((tiff = TIFFOpen([tiffPath UTF8String], "w")) == NULL) {
                NSLog(@"Unable to write to file.");
                return nRet = AVIPDF_FALSE;
            }
            
            size_t width = CGImageGetWidth(srcCGImage);
            size_t height = CGImageGetHeight(srcCGImage);
            
            TIFFSetField(tiff, TIFFTAG_IMAGEWIDTH, width);
            TIFFSetField(tiff, TIFFTAG_IMAGELENGTH, height);
            TIFFSetField(tiff, TIFFTAG_BITSPERSAMPLE, 1);
            TIFFSetField(tiff, TIFFTAG_SAMPLESPERPIXEL, 1);
            TIFFSetField(tiff, TIFFTAG_ROWSPERSTRIP, 1);
            
            if (stPdfData->nCompressRate == AVIPDF_COMPRESSRATE_G31D) {
                TIFFSetField(tiff, TIFFTAG_COMPRESSION, COMPRESSION_CCITTFAX3);
            } else if (stPdfData->nCompressRate == AVIPDF_COMPRESSRATE_RUNLENGTH){
                TIFFSetField(tiff, TIFFTAG_COMPRESSION, COMPRESSION_CCITTRLE);
            }
            
            TIFFSetField(tiff, TIFFTAG_FILLORDER, FILLORDER_MSB2LSB);
            TIFFSetField(tiff, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
            
            TIFFSetField(tiff, TIFFTAG_XRESOLUTION, 200.0);
            TIFFSetField(tiff, TIFFTAG_YRESOLUTION, 200.0);
            TIFFSetField(tiff, TIFFTAG_RESOLUTIONUNIT, RESUNIT_INCH);
            
            unsigned char *ptr = pixelDataPtr; // initialize pointer to the first byte of the image buffer
            unsigned char red, green, blue, gray, eightPixels;
            tmsize_t bytesPerStrip = ceil(width/8.0);
            unsigned long long total = 0;
            unsigned char *strip = (unsigned char *)_TIFFmalloc(bytesPerStrip);
            
            for (int y=0; y<height; y++) {
                for (int x=0; x<width; x++) {
                    red = *ptr++; green = *ptr++; blue = *ptr++;
                    ptr++; // discard fourth byte by advancing the pointer 1 more byte
                    gray = .3 * red + .59 * green + .11 * blue; // http://answers.yahoo.com/question/index?qid=20100608031814AAeBHPU
                    eightPixels = strip[x/8];
                    eightPixels = eightPixels << 1;
                    if (gray < threshold) eightPixels = eightPixels | 1; // black=1 in tiff image without TIFFTAG_PHOTOMETRIC header
                    strip[x/8] = eightPixels;
                }
                
                TIFFWriteEncodedStrip(tiff, y, strip, bytesPerStrip);
                total += bytesPerStrip;
            }
            TIFFClose(tiff);
            NSLog(@"%llu", total);
            if (strip) _TIFFfree(strip);
            if (pixelData) CFRelease(pixelData);
            TIFF *tif = TIFFOpen([tiffPath UTF8String], "r");
            
            if (tif) {
                unsigned char *buf;
                tstrip_t strip;
                unsigned long long *bc;
                unsigned int stripsize;
                
                TIFFGetField(tif, TIFFTAG_STRIPBYTECOUNTS, &bc);
                stripsize = (unsigned int)bc[0];
                buf = _TIFFmalloc(stripsize);
                for (strip = 0; strip < TIFFNumberOfStrips(tif); strip++) {
                    if (bc[strip] > stripsize) {
                        buf = _TIFFrealloc(buf, (unsigned int)bc[strip]);
                        stripsize = (unsigned int)bc[strip];
                    }
                    signed int returnedSize = TIFFReadRawStrip(tif, strip, buf, (unsigned int)bc[strip]);
                    pdf_outputData(stPdfData, buf, returnedSize);
                }
                _TIFFfree(buf);
                TIFFClose(tif);
            }
        }
#ifdef ENABLE_BW_COMPRESSION
        if(stPdfData->nCompressRate == AVIPDF_COMPRESSRATE_RUNLENGTH)
        {
            for(ptr1 = szImageData, i = 0; i < lines; i++)
            {
                nRet = RunLengthEncode(ptr1, nImgWidthInBytes, pDataCompressed);
                pdf_outputData(stPdfData, pDataCompressed, nRet);
                ptr1 += nImgWidthInBytes;
            }
        }
        else if(stPdfData->nCompressRate == AVIPDF_COMPRESSRATE_G31D)
        {
            for (ptr1 = szImageData, i = 0; i < lines; i++) {
                nRet = G3Encode_1D(ptr1, nImgWidthInBytes, stPdfData->nImageWidth, pDataCompressed, 1, 0);
                pdf_outputData(stPdfData, pDataCompressed, nRet);
                ptr1 += nImgWidthInBytes;
            }
        }
        else if(stPdfData->nCompressRate == AVIPDF_COMPRESSRATE_G32D)
        {
            for (ptr1 = szImageData, i = 0; i < lines; i = i + K) {
                nRet = G3Encode_1D(ptr1, nImgWidthInBytes, stPdfData->nImageWidth, pDataCompressed, 1, 1);
                pdf_outputData(stPdfData, pDataCompressed, nRet);
                for (j = 0; j < (K - 1); j++) {
                    nRet = G3Encode_2D(ptr1, ptr1 + nImgWidthInBytes, nImgWidthInBytes, stPdfData->nImageWidth, pDataCompressed, 1);
                    pdf_outputData(stPdfData, pDataCompressed, nRet);
                    ptr1 += nImgWidthInBytes;
                }
                ptr1 += nImgWidthInBytes;
            }
        }
        else if(stPdfData->nCompressRate == AVIPDF_COMPRESSRATE_G4)
        {
#if 0
            if(gpRefTmp == NULL)
            {
                gpRefTmp = malloc(nImgWidthInBytes);
                memset(gpRefTmp, 0x00, nImgWidthInBytes);
            }
            pRef = gpRefTmp;
#endif
            pRef = gRefLine;
            for (ptr1 = szImageData, i = 0; i < lines; i++) {
                nRet = G3Encode_MMR(pRef, ptr1, nImgWidthInBytes, stPdfData->nImageWidth, pDataCompressed);
                pdf_outputData(stPdfData, pDataCompressed, nRet);
                pRef = ptr1;
                ptr1 += nImgWidthInBytes;
                stPdfData->nStripByteCounts = stPdfData->nStripByteCounts + nRet;
            }
            stPdfData->nRemainHight = stPdfData->nRemainHight - lines;
            G3Encode_MMRF_GetLastByte(pDataCompressed);
            memcpy(gRefLine, ptr1 - nImgWidthInBytes, nImgWidthInBytes);
            if(stPdfData->nRemainHight <= 0)
            {
                nRet = G3EncodeEnd_MMR(pDataCompressed, stPdfData->nStripByteCounts);
                pdf_outputData(stPdfData, pDataCompressed, nRet);
            }
        }
#endif
    }
    else if(stPdfData->nColorType == AVIPDF_COLORTYPE_GRAY || stPdfData->nColorType == AVIPDF_COLORTYPE_COLOR)
    {
        if(stPdfData->nSourceFileFormat == AVIPDF_SOURCEFILEFORMAT_RAW)
        {
            NSError *error = nil;
            UIImage *imageToSave = [AVISmartImageConverter convertToImage:szImageData width:stPdfData->nImageWidth height:stPdfData->nImageHeight options:AVISmartImageConvertOptionsNone error:&error];
            if (stPdfData->nColorType == AVIPDF_COLORTYPE_GRAY) {
                imageToSave = imageToSave.grayScaleImage;
            }
            NSData *imageData = UIImageJPEGRepresentation(imageToSave, 0.2);
            unsigned char *imageDataBuffer = malloc(imageData.length);
            [imageData getBytes:imageDataBuffer length:imageData.length];
            pdf_outputData(stPdfData, imageDataBuffer, (int)imageData.length);
            
            free(imageDataBuffer);
            //nLen = JPEGWriteData(stPdfData->sJpegData, szImageData, nImageDataLen);
            //nLen = nLen - stPdfData->nCompSize;
            //stPdfData->nCompSize = stPdfData->nCompSize + nLen;
            //stPdfData->nBufferCurIndex = stPdfData->nBufferCurIndex + nLen;
        }
    }

    if(pDataCompressed != NULL)
        free(pDataCompressed);

    nRet = AVIPDF_TRUE;
    return nRet;
}

int pdf_WriteData(PDFDATA * stPdfData, unsigned char ** pOutputData, int * nOutputDataSize, unsigned char *szImageData, int nImageDataLen)
{
    int nRet = AVIPDF_FALSE;
    int nImgWidthInBytes = 0;
    int nShortLineLen = 0;
    int nLine = 0;
    unsigned char * pPtr = NULL;
    int nUsedLen = 0, nUnUsedLen = 0;

    *pOutputData = NULL;

    if(stPdfData == NULL)
        return nRet;
    
    if(stPdfData->nColorType == AVIPDF_COLORTYPE_BW || (stPdfData->nColorType != AVIPDF_COLORTYPE_BW && stPdfData->nSourceFileFormat == AVIPDF_SOURCEFILEFORMAT_RAW))
    {
        if(stPdfData->nColorType == AVIPDF_COLORTYPE_BW)
        {
            if(stPdfData->nCompressRate == AVIPDF_COMPRESSRATE_G32D)
                nImgWidthInBytes = ((stPdfData->nImageWidth + 7) / 8) * 2;
            else
                nImgWidthInBytes = ((stPdfData->nImageWidth + 7) / 8);
        }
        else if(stPdfData->nColorType == AVIPDF_COLORTYPE_GRAY)
            nImgWidthInBytes = stPdfData->nImageWidth;
        else if(stPdfData->nColorType == AVIPDF_COLORTYPE_COLOR)
            nImgWidthInBytes = stPdfData->nImageWidth * 3;

        if(stPdfData->pUnUseBuffer != NULL && stPdfData->nUnUseBufferIndex != 0)
        {
            if(stPdfData->nUnUseBufferIndex + nImageDataLen >= nImgWidthInBytes)
            {
           	    nShortLineLen = nImgWidthInBytes - stPdfData->nUnUseBufferIndex;
               	memcpy(&stPdfData->pUnUseBuffer[stPdfData->nUnUseBufferIndex], szImageData, nShortLineLen);
               	pdf_WriteDataLine(stPdfData, stPdfData->pUnUseBuffer, nImgWidthInBytes);
               	memset(stPdfData->pUnUseBuffer, 0x00, stPdfData->nUnUseBufferSize);
           	    stPdfData->nUnUseBufferIndex = 0;
               	pPtr = &szImageData[nShortLineLen];
            }
            else
            {
            	memcpy(&stPdfData->pUnUseBuffer[stPdfData->nUnUseBufferIndex], szImageData, nImageDataLen);
            	stPdfData->nUnUseBufferIndex = stPdfData->nUnUseBufferIndex + nImageDataLen;
                nRet = AVIPDF_TRUE;
            	return nRet;
            }
        }
        else
        {
    	    nShortLineLen = 0;
        	pPtr = szImageData;
        }
        if((nImageDataLen - nShortLineLen) > 0)
        {
            nLine = (nImageDataLen - nShortLineLen) / nImgWidthInBytes;
            if(nLine > 0)
            {
                pdf_WriteDataLine(stPdfData, pPtr, nImgWidthInBytes * nLine);
            }
            nUsedLen = nShortLineLen + (nImgWidthInBytes * nLine);
            if(nUsedLen <  nImageDataLen)
            {
                nUnUsedLen = nImageDataLen - nUsedLen;
                if(nUnUsedLen > 0)
                {
                    memcpy(stPdfData->pUnUseBuffer, &szImageData[nUsedLen], nUnUsedLen);
                    stPdfData->nUnUseBufferIndex = nUnUsedLen;
                }
            }
        }
        if(stPdfData->nOutputBufferIndex > stPdfData->nOutputBufferReadIndex)
        {
            *pOutputData = &stPdfData->pOutputBuffer[stPdfData->nOutputBufferReadIndex];
            *nOutputDataSize = stPdfData->nOutputBufferIndex - stPdfData->nOutputBufferReadIndex;
            stPdfData->nOutputBufferReadIndex = stPdfData->nOutputBufferIndex;
        }
        else
        {
            *pOutputData = NULL;
            *nOutputDataSize = 0;
        }
    }
    else
    {
        if(stPdfData->nSourceFileFormat == AVIPDF_SOURCEFILEFORMAT_RAW)
        {
            pdf_WriteDataLine(stPdfData, szImageData, nImageDataLen);
            if(stPdfData->nOutputBufferIndex > stPdfData->nOutputBufferReadIndex)
            {
                *pOutputData = &stPdfData->pOutputBuffer[stPdfData->nOutputBufferReadIndex];
                *nOutputDataSize = stPdfData->nOutputBufferIndex - stPdfData->nOutputBufferReadIndex;
                stPdfData->nOutputBufferReadIndex = stPdfData->nOutputBufferIndex;
            }
            else
            {
                *pOutputData = NULL;
                *nOutputDataSize = 0;
            }
        }
        else
        {
            *pOutputData = szImageData;
            *nOutputDataSize = nImageDataLen;
            stPdfData->nOutputFileIndex = stPdfData->nOutputFileIndex + nImageDataLen;
        }
    }

    nRet = AVIPDF_TRUE;
    return nRet;
}

int pdf_EndPage(PDFDATA * stPdfData, unsigned char ** pOutputData, int * nOutputDataSize)
{
    int nRet = AVIPDF_FALSE;
    char buf[256];
    char RTC_1D[]={0x00, 0x10, 0x01, 0x00, 0x10, 0x01, 0x00, 0x10, 0x01};

    *pOutputData = NULL;
    
    if(stPdfData == NULL)
        return nRet;

    if(stPdfData->nColorType == AVIPDF_COLORTYPE_BW) 
    {
//        if(stPdfData->pUnUseBuffer != NULL && stPdfData->nUnUseBufferIndex != 0)
//        {
//            pdf_WriteDataLine(stPdfData, stPdfData->pUnUseBuffer, stPdfData->nUnUseBufferIndex);
//        }
        if(stPdfData->nCompressRate == AVIPDF_COMPRESSRATE_NONE)
        {
            pdf_outputData(stPdfData, " ", 1);
        }
        else if(stPdfData->nCompressRate == AVIPDF_COMPRESSRATE_RUNLENGTH)
        {
            pdf_outputData(stPdfData, " ", 1);
        }
        else if(stPdfData->nCompressRate == AVIPDF_COMPRESSRATE_G31D)
        {
            pdf_outputData(stPdfData, RTC_1D, 9);
        }
        else if(stPdfData->nCompressRate == AVIPDF_COMPRESSRATE_G32D)
        {
            pdf_outputData(stPdfData, RTC_1D, 9);
        }
        else if(stPdfData->nCompressRate == AVIPDF_COMPRESSRATE_G4)
        {
#if 0
            if(gpRefTmp != NULL)
                free(gpRefTmp);
#endif
            pdf_outputData(stPdfData, RTC_1D, 9);
        }
    }
    else if(stPdfData->nColorType == AVIPDF_COLORTYPE_GRAY || stPdfData->nColorType == AVIPDF_COLORTYPE_COLOR)
    {
        if(stPdfData->nSourceFileFormat == AVIPDF_SOURCEFILEFORMAT_RAW)
        {
            pdf_outputData(stPdfData, " ", 1);
            //nLen = JPEGEndPage(stPdfData->sJpegData);
            //nLen = nLen - stPdfData->nCompSize;
            //stPdfData->nCompSize = stPdfData->nCompSize + nLen;
            //stPdfData->nBufferCurIndex = stPdfData->nBufferCurIndex + nLen;

            //stPdfData->nImgSize = JPEGEndFileB(stPdfData->sJpegData);
            //nLen = stPdfData->nImgSize - stPdfData->nCompSize;
            //stPdfData->nCompSize = stPdfData->nCompSize + nLen;
            //stPdfData->nBufferCurIndex = stPdfData->nBufferCurIndex + nLen;
        }
    }

    pdf_outputData(stPdfData, pdfXObjectE, strlen(pdfXObjectE));
    
    stPdfData->nObjIndex += 4;;
    XrefTable[stPdfData->nObjIndex].offset = stPdfData->nOutputFileIndex;
    XrefTable[stPdfData->nObjIndex].generation = 0;
    XrefTable[stPdfData->nObjIndex].type = 'n';
    sprintf(buf, pdfLength, stPdfData->nObjIndex, stPdfData->nOutputFileIndex);
    pdf_outputData(stPdfData, buf, strlen(buf));

    if(stPdfData->nOutputBufferIndex > stPdfData->nOutputBufferReadIndex)
    {
        *pOutputData = &stPdfData->pOutputBuffer[stPdfData->nOutputBufferReadIndex];
        *nOutputDataSize = stPdfData->nOutputBufferIndex - stPdfData->nOutputBufferReadIndex;
        stPdfData->nOutputBufferReadIndex = stPdfData->nOutputBufferIndex;
    }
    else
    {
        *pOutputData = NULL;
        *nOutputDataSize = 0;
    }

    if(stPdfData->pUnUseBuffer != NULL)
    {
        if(stPdfData->pUnUseBuffer != NULL)
        {
        	free(stPdfData->pUnUseBuffer);
            stPdfData->pUnUseBuffer = NULL;
        }
        stPdfData->nUnUseBufferIndex = 0;
        stPdfData->nUnUseBufferSize = 0;
    }

    stPdfData->nOutputBufferIndex = 0;
    stPdfData->nOutputBufferReadIndex = 0;
    
    nRet = AVIPDF_TRUE;
    return nRet;
}

int pdf_EndFile(PDFDATA * stPdfData, unsigned char ** pOutputData, int * nOutputDataSize)
{
    int nRet = AVIPDF_FALSE;
    char buf[512], trailer[128], szTime[64];
    char kids[10];
    int i;
    time_t timer;
    struct tm *ptm;

    *pOutputData = NULL;
    
    if(stPdfData == NULL)
        return nRet;
    
    XrefTable[3].offset = stPdfData->nOutputFileIndex;
    XrefTable[3].generation = 0;
    XrefTable[3].type = 'n';
    pdf_outputData(stPdfData, pdfPageTreeB, strlen(pdfPageTreeB));
    
    memset(szPageTree, 0x00, sizeof(szPageTree));
    for(i=0; i < stPdfData->nPageCount; i++)
    {
        sprintf(kids, "%d 0 R", 4+i*5);
        strcat(szPageTree, kids);
        strcat(szPageTree, " ");
        if((i%50) == 49 && i<(stPdfData->nPageCount-1))
        {
            pdf_outputData(stPdfData, szPageTree, strlen(szPageTree));
            memset(szPageTree, 0x00, sizeof(szPageTree));
        }
    }

    pdf_outputData(stPdfData, szPageTree, strlen(szPageTree));
    
    sprintf(szPageTree, pdfPageTreeE, stPdfData->nPageCount);
    pdf_outputData(stPdfData, szPageTree, strlen(szPageTree));

    XrefTable[1].offset = stPdfData->nOutputFileIndex;
    XrefTable[1].generation = 0;
    XrefTable[1].type = 'n';
    pdf_outputData(stPdfData, pdfCatalog, strlen(pdfCatalog));

    XrefTable[2].offset = stPdfData->nOutputFileIndex;
    XrefTable[2].generation = 0;
    XrefTable[2].type = 'n';
    timer = (time_t) time(NULL);
    ptm = localtime(&timer);
    sprintf(szTime,"     ");
    sprintf(buf, pdfInfo, stPdfData->szArtist, szTime);
    pdf_outputData(stPdfData, buf, strlen(buf));

    stPdfData->nObjIndex++;
    sprintf(trailer, pdfTrailer, stPdfData->nObjIndex, stPdfData->nOutputFileIndex);
    
    sprintf(buf, pdfXrefTable, stPdfData->nObjIndex);
    pdf_outputData(stPdfData, buf, strlen(buf));

    for(i=0; i < stPdfData->nObjIndex; i++)
    {
        sprintf(buf, "%010d %05d %c\r\n", XrefTable[i].offset, XrefTable[i].generation, XrefTable[i].type);
        pdf_outputData(stPdfData, buf, strlen(buf));
    }

    pdf_outputData(stPdfData, trailer, strlen(trailer));

    if(stPdfData->nOutputBufferIndex > stPdfData->nOutputBufferReadIndex)
    {
        *pOutputData = &stPdfData->pOutputBuffer[stPdfData->nOutputBufferReadIndex];
        *nOutputDataSize = stPdfData->nOutputBufferIndex - stPdfData->nOutputBufferReadIndex;
        stPdfData->nOutputBufferReadIndex = stPdfData->nOutputBufferIndex;
    }
    else
    {
        *pOutputData = NULL;
        *nOutputDataSize = 0;
    }
    
    nRet = AVIPDF_TRUE;
    return nRet;
}

int pdf_CurFileSize(PDFDATA * stPdfData)
{
    int nRet = AVIPDF_FALSE;
    
    if(stPdfData == NULL)
        return nRet;

    nRet = stPdfData->nOutputFileIndex;

    return nRet;
}

unsigned long long pdf_offsetForPage(int page) {
    
    return 123;
}

