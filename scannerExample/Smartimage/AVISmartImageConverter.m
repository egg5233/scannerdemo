//
//  AVISmartImageConverter.m
//  Avision-Smart-Image-Demo
//
//  Created by Youwei Teng on 12/04/2017.
//  Copyright © 2017 Youwei Teng. All rights reserved.
//

#import "AVISmartImageConverter.h"
#import "UIImage+BitmapBuffer.h"

@implementation AVISmartImageConverter

+ (UIImage *)convertToImage:(AVISmartImage *)image options:(AVISmartImageConvertOptions)options error:(NSError**)outError {
    
    if (options == AVISmartImageConvertOptionsGray) {
        return [UIImage convertBitmapRGBA8ToUIImage:image.rawBuffer withWidth:image.width withHeight:image.height].grayScaleImage;
    } else {
        return [UIImage convertBitmapRGBA8ToUIImage:image.rawBuffer withWidth:image.width withHeight:image.height];
    }
}

+ (UIImage *)convertToImage:(unsigned char *)imageBuffer width:(int)width height:(int)height options:(AVISmartImageConvertOptions)options error:(NSError**)outError {
    UIImage *result = [UIImage convertBitmapRGBA8ToUIImage:imageBuffer withWidth:width withHeight:height];
    return result;
}

+ (CGImageRef)convertToCGImage:(AVISmartImage *)image options:(AVISmartImageConvertOptions)options error:(NSError**)outError {
    UIImage *result = [AVISmartImageConverter convertToImage:image options:options error:outError];
    return result.CGImage;
}

+ (AVISmartImage *)convertFromImage:(UIImage *)image options:(AVISmartImageConvertOptions)options error:(NSError**)outError {
    NSData *data = [NSData dataWithBytes:[UIImage convertToBitmapRGBA8:image] length:image.size.width * image.size.height * 4];
    AVISmartImage *avImage = [[AVISmartImage alloc] initWithData:data width:image.size.width height:image.size.height bitsPerPixel:32 order:AVICodecLoadByteOrderRGBA error:outError];
    return avImage;
}

+ (AVISmartImage *)convertFromCGImage:(CGImageRef)image options:(AVISmartImageConvertOptions)options error:(NSError**)outError {
    return nil;
}

@end
