//
//  AVISmartImageColor.h
//  Avision-Smart-Image-Demo
//
//  Created by Youwei Teng on 12/04/2017.
//  Copyright © 2017 Youwei Teng. All rights reserved.
//
#import <Foundation/Foundation.h>

@interface AVISmartImageColor : NSObject<NSCopying>

@property (nonatomic, assign) unsigned char r;
@property (nonatomic, assign) unsigned char g;
@property (nonatomic, assign) unsigned char b;
@property (nonatomic, assign) unsigned char a;
@property (nonatomic, assign) unsigned char reserved;

- (instancetype)copyWithZone:(NSZone*)zone;

+ (instancetype)colorWithRed:(int)red green:(int)green blue:(int)blue alpha:(int)alpha;
+ (instancetype)colorWithRed:(int)red green:(int)green blue:(int)blue;
+ (instancetype)white;
+ (instancetype)black;

@end

