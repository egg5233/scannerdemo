//
//  AVISmartImageStream.h
//  Avision-Smart-Image-Demo
//
//  Created by Youwei Teng on 12/04/2017.
//  Copyright © 2017 Youwei Teng. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AVISmartImageStream : NSObject


@property (nonatomic, strong) NSData *data;

// TODO: implement
@property (nonatomic, strong) NSString *filePath;
@property (nonatomic, strong) NSURL *fileURL;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithFileAtPath:(NSString *)filePath;
- (instancetype)initWithData:(NSData *)data NS_DESIGNATED_INITIALIZER;

@end
