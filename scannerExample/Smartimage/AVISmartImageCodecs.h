//
//  AVISmartImageCodecs.h
//  Avision-Smart-Image-Demo
//
//  Created by Youwei Teng on 12/04/2017.
//  Copyright © 2017 Youwei Teng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AVISmartImage.h"
#import "AVISmartImageStream.h"
#import "AVISmartImageDefines.h"
#import "pdf.h"

@interface AVISmartImageCodecs : NSObject

- (AVISmartImage *)load:(AVISmartImageStream *)stream error:(NSError **)error;

- (AVISmartImage *)load:(AVISmartImageStream *)stream width:(unsigned long)width height:(unsigned long)height colorMode:(AVISmartImageColorMode)colorMode order:(AVICodecLoadByteOrder)order firstPage:(unsigned long)firstPage lastPage:(unsigned long)lastPage error:(NSError **)error;

// for PDF
- (AVISmartImage *)load:(AVISmartImageStream *)stream pageNumber:(unsigned long)pageNumber error:(NSError **)error;

- (AVISmartImage *)load:(AVISmartImageStream *)stream colorMode:(AVISmartImageColorMode)colorMode order:(AVICodecLoadByteOrder)order error:(NSError **)error;

- (BOOL)save:(AVISmartImage *)image stream:(AVISmartImageStream *)stream format:(AVISmartImageFormat)format colorMode:(AVISmartImageColorMode)colorMode error:(NSError**)error;

- (BOOL)save:(AVISmartImage *)image stream:(AVISmartImageStream *)stream format:(AVISmartImageFormat)format colorMode:(AVISmartImageColorMode)colorMode firstPage:(unsigned int)firstPage lastPage:(int)lastPage firstSavePageNumber:(unsigned int)firstSavePageNumber pageMode:(AVISmartImageCodecSavePageMode)pageMode error:(NSError**)error;

// for PDF
//- (PDFDATA *)startPDF:(AVISmartImage *)image toStream:(AVISmartImageStream *)stream;
- (PDFDATA *)startPDFWithStream:(AVISmartImageStream *)stream;
- (void)appendImage:(AVISmartImage *)image inPDF:(PDFDATA *)pdfData colorMode:(AVISmartImageColorMode)colorMode;
- (void)endPDF:(PDFDATA *)pdfData;

@end
