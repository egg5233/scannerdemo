//
//  AVISmartImageConverter.h
//  Avision-Smart-Image-Demo
//
//  Created by Youwei Teng on 12/04/2017.
//  Copyright © 2017 Youwei Teng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "AVISmartImage.h"

typedef NS_OPTIONS(NSUInteger, AVISmartImageConvertOptions) {
    AVISmartImageConvertOptionsNone      = (1 << 0),
    AVISmartImageConvertOptionsKeepAlpha = (1 << 1),
    AVISmartImageConvertOptionsGray      = (1 << 2),
    AVISmartImageConvertOptionsBW        = (1 << 3)
};

@interface AVISmartImageConverter : NSObject

+ (UIImage *)convertToImage:(AVISmartImage *)image options:(AVISmartImageConvertOptions)options error:(NSError**)outError;
+ (CGImageRef)convertToCGImage:(AVISmartImage *)image options:(AVISmartImageConvertOptions)options error:(NSError**)outError;
+ (UIImage *)convertToImage:(unsigned char *)imageBuffer width:(int)width height:(int)height options:(AVISmartImageConvertOptions)options error:(NSError**)outError;
+ (AVISmartImage *)convertFromImage:(UIImage *)image options:(AVISmartImageConvertOptions)options error:(NSError**)outError;
+ (AVISmartImage *)convertFromCGImage:(CGImageRef)image options:(AVISmartImageConvertOptions)options error:(NSError**)outError;

@end
