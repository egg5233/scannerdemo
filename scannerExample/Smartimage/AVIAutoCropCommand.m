//
//  AVIAutoCropCommand.m
//  Avision-Smart-Image-Demo
//
//  Created by Youwei Teng on 12/04/2017.
//  Copyright © 2017 Youwei Teng. All rights reserved.
//

#import "AVIAutoCropCommand.h"

@implementation AVIAutoCropCommand

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (instancetype)initWithOutMode:(AVISmartImageOutMode)mode adjustEdge:(UIEdgeInsets)edgeInsets {
    self = [self init];
    if (self) {
        self.outMode = mode;
        self.edgeInsets = edgeInsets;
    }
    return self;
}

- (BOOL)run:(AVISmartImage *)inputImage error:(NSError *__autoreleasing *)error {
    [super run:inputImage error:error];
    // pLNIDATA for DeskewCropProc
    long status[1] = {0};
    NIDATA plnidata = {
        .Size = sizeof(NIDATA),
        .Model = "0A230638",
        .InImage = {
            .ScanType = inputImage.scanType, // adf
            .ScanMode = inputImage.scanMode, // color
            .XRes = inputImage.xResolution,
            .YRes = inputImage.yResolution,
            .Pixel = (unsigned int)inputImage.width,
            .Line = (unsigned int)inputImage.height,
            .BitsPerPixel = 24, // grey = 8, color = 24
            .SamplePerPixel = 3,
            .BytesPerLine = (unsigned int)inputImage.bytesPerLine,  // exclude alpha
            .ColorFilter = 0,
            .BackgroundColor = 0,
            .x = 0,
            .y = 0,
            .ScanTypeMaxWidth = inputImage.width + 100, // not sure how many pixel
            .CurPages = 1
        },
        .OutImage = {
            .OutMode = (0 << 2), // color
            .Gray = {
                .BackgroundColor = 75,
            },
            .Color = {
                .BackgroundColorR = 75,
                .BackgroundColorG = 75,
                .BackgroundColorB = 75
            },
            .AutoCrop = 0x01,
            .Rotate = 1, // deskew
            .AdjCrop = {
                .Top = self.edgeInsets.top,
                .Left = self.edgeInsets.left,
                .Bottom = self.edgeInsets.bottom,
                .Right = self.edgeInsets.right,
            }
        }
    };
    
    unsigned char *buffer = inputImage.rawBuffer;
    unsigned char *rgbBuffer = NULL;
    unsigned char *desBuffer = NULL;
//    if (inputImage.order == AVICodecLoadByteOrderRGBA) {
    // Convert RGBA to RGB buffer
    // Smart Image only support 24 bit
    unsigned long smartImageRGBBufferLength = inputImage.width * inputImage.height * 3;
    unsigned long bytesPerRow = inputImage.width * 4 /* has alpha = 4, no alpha = 3 */;
    // Allocate buffer memeory
    rgbBuffer = malloc(smartImageRGBBufferLength);
    desBuffer = malloc(smartImageRGBBufferLength);
    
    // Skip alpha channel to create 24 bit RGB buffer
    int rgbCount = 0;
    for (int i = 0; i < inputImage.height; i++) {
        for (int j = 0; j < bytesPerRow; j += 4) {
            unsigned long current = i * bytesPerRow + j;
            rgbBuffer[rgbCount] = buffer[current];
            rgbBuffer[rgbCount + 1] = buffer[current + 1];
            rgbBuffer[rgbCount + 2] = buffer[current + 2];
            rgbCount += 3;
        }
    }
//    }
    
    // Run Smart Image process
    
    [[AVISmartImageHelper new] DeskewCropProc:rgbBuffer and:(void **)&desBuffer and:&plnidata and:NULL and:status];
    
    // iOS only support 32 bit RGBA buffer, add alpha channel back
    unsigned long size = inputImage.width * inputImage.height;
    unsigned long outputBufferLength = size * 4;
    unsigned char *rgbaBuffer = malloc(outputBufferLength);
    for(int i = 0; i < size; i++) {
        rgbaBuffer[4 * i]     = desBuffer[3 * i];       // R
        rgbaBuffer[4 * i + 1] = desBuffer[3 * i + 1];   // G
        rgbaBuffer[4 * i + 2] = desBuffer[3 * i + 2];   // B
        rgbaBuffer[4 * i + 3] = (char)255;              // A
    }
    
    NSLog(@"%@ completed, status = %ld", NSStringFromClass([self class]), *status);
    NSLog(@"Output Image width = %u, height = %u", plnidata.OutImage.ImageWidth, plnidata.OutImage.ImageHeight);
    inputImage.data = [NSData dataWithBytes:rgbaBuffer length:outputBufferLength];
    // update the new width and height
    inputImage.width = plnidata.OutImage.ImageWidth;
    inputImage.height = plnidata.OutImage.ImageHeight;
    
    free(rgbBuffer);
    free(desBuffer);
    free(rgbaBuffer);
    
    return YES;
}

@end
