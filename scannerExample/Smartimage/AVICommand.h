//
//  AVICommand.h
//  Avision-Smart-Image-Demo
//
//  Created by Youwei Teng on 12/04/2017.
//  Copyright © 2017 Youwei Teng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AVISmartImage.h"
#import "AVISmartImageDefines.h"
#import "AVISmartImageHelper.h"
#import "Ios.h"

@interface AVICommand : NSObject

@property (nonatomic, assign) BOOL hasProgress;

- (BOOL)run:(AVISmartImage *)inputImage error:(NSError **)error;

@end

