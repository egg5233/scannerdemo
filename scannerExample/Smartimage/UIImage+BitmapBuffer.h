//
//  UIImage+BitmapBuffer.h
//  Avision-Smart-Image-Demo
//
//  Created by Youwei Teng on 10/04/2017.
//  Copyright © 2017 Youwei Teng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIImage (BitmapBuffer)

+ (unsigned char *)convertToBitmapRGBA8:(UIImage *)image;

+ (CGContextRef)newBitmapRGBA8ContextFromImage:(CGImageRef)image;

+ (UIImage *)convertBitmapRGBA8ToUIImage:(unsigned char *)buffer
                               withWidth:(unsigned long)width
                              withHeight:(unsigned long)height;
+ (UIImage *)convertBitmapBWToUIImage:(unsigned char *)buffer
                               withWidth:(unsigned long)width
                              withHeight:(unsigned long)height;
- (size_t)RGBA8bufferLength;

- (uint8_t *)grayBuffer;
- (UIImage *)grayScaleImage;
@end
