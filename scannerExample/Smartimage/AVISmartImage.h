//
//  AVISmartImage.h
//  Avision-Smart-Image-Demo
//
//  Created by Youwei Teng on 12/04/2017.
//  Copyright © 2017 Youwei Teng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "AVISmartImageColor.h"
#import "AVISmartImageDefines.h"

@interface AVISmartImage : NSObject

- (instancetype)init NS_UNAVAILABLE;

- (instancetype)initWithData:(NSData*)data width:(unsigned long)width height:(unsigned long)height bitsPerPixel:(unsigned int)bitsPerPixel order:(AVICodecLoadByteOrder)order error:(NSError**)error NS_DESIGNATED_INITIALIZER;

- (instancetype)clone;

// static methods
+ (instancetype)createColoredWithWidth:(unsigned int)inWidth height:(unsigned int)inHeight bitsPerPixel:(unsigned int)bitsPerPixel resolution:(unsigned int)resolution backgroundColor:(AVISmartImageColor *)backgroundColor error:(NSError**)error;

@property (nonatomic, assign) unsigned long width;
@property (nonatomic, assign) unsigned long height;

@property (nonatomic, assign) unsigned int bitsPerPixel;
@property (nonatomic, assign) unsigned long bytesPerLine;
@property (nonatomic, assign) unsigned int xResolution;
@property (nonatomic, assign) unsigned int yResolution;
@property (nonatomic, assign) unsigned int samplePerPixel;
@property (nonatomic, assign) unsigned int colorFilter;
@property (nonatomic, assign) unsigned int backgroundColor;

// TODO:
@property (nonatomic, assign) AVISmartImageScanMode scanMode;

@property (nonatomic, assign) AVICodecLoadByteOrder order;
@property (nonatomic, assign) unsigned char *rawBuffer;
@property (nonatomic, strong) NSData *data;
@property (nonatomic, assign) AVISmartImageScanType scanType;

@end

