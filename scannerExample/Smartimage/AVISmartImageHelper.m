//
//  AVISmartImageHelper.m
//  Avision-Smart-Image-Demo
//
//  Created by Youwei Teng on 12/04/2017.
//  Copyright © 2017 Youwei Teng. All rights reserved.
//

#import "AVISmartImageHelper.h"
#import "AVIUtils.h"

char * SI_SERIAL_NUMBER = "D5D50D54-6B89-488f-A9F0-C2C104D8EC9A";

@implementation AVISmartImageHelper

+ (id)shared {
    static AVISmartImageHelper *sharedHelper = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedHelper = [[self alloc] init];
        NSString *path = [AVIUtils documentFolderPath];
        char *cpath = strdup([path UTF8String]);
        long result = [sharedHelper InitializeLibProc:SI_SERIAL_NUMBER];
        NSLog(@"SmartImageSL InitializeLibProc serial %s, result = %ld", SI_SERIAL_NUMBER, result);
        NSLog(@"SmartImageSL SetDebugMode, result = %ld", [sharedHelper SetDebugMode:0x40000005 and:cpath]);
        NSLog(@"debug path = %@", path);
        free(cpath);
    });
    return sharedHelper;
}

- (id)init {
    if (self = [super init]) {
        NSString *path = [AVIUtils documentFolderPath];
        char *cpath = strdup([path UTF8String]);
        long result = [self InitializeLibProc:SI_SERIAL_NUMBER];
        NSLog(@"SmartImageSL InitializeLibProc serial %s, result = %ld", SI_SERIAL_NUMBER, result);
        NSLog(@"SmartImageSL SetDebugMode, result = %ld", [self SetDebugMode:0x40000005 and:cpath]);
        NSLog(@"debug path = %@", path);
        free(cpath);
    }
    return self;
}

- (void)dealloc {
    [self TerminateLibProc];
}

@end
