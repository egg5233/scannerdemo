//
//  AVIAutoCropCommand.h
//  Avision-Smart-Image-Demo
//
//  Created by Youwei Teng on 12/04/2017.
//  Copyright © 2017 Youwei Teng. All rights reserved.
//

#import "AVICommand.h"
#import <UIKit/UIKit.h>
@interface AVIAutoCropCommand : AVICommand

@property (nonatomic, assign) AVISmartImageOutMode outMode;
@property (nonatomic, assign) UIEdgeInsets edgeInsets;

- (instancetype)initWithOutMode:(AVISmartImageOutMode)mode adjustEdge:(UIEdgeInsets)edgeInsets;

@end
