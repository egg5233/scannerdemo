//
//  AVISmartImageColor.m
//  Avision-Smart-Image-Demo
//
//  Created by Youwei Teng on 12/04/2017.
//  Copyright © 2017 Youwei Teng. All rights reserved.
//

#import "AVISmartImageColor.h"

@implementation AVISmartImageColor

+ (instancetype)colorWithRed:(int)red green:(int)green blue:(int)blue alpha:(int)alpha {
    AVISmartImageColor *color = [AVISmartImageColor new];
    color.r = red;
    color.g = green;
    color.b = blue;
    color.a = alpha;
    
    return color;
}


+ (instancetype)colorWithRed:(int)red green:(int)green blue:(int)blue {
    return [AVISmartImageColor colorWithRed:red green:green blue:blue alpha:255];
}

+ (instancetype)white {
    return [AVISmartImageColor colorWithRed:1 green:1 blue:1 alpha:255];
}

+ (instancetype)black {
    return [AVISmartImageColor colorWithRed:0 green:0 blue:0 alpha:255];
}

- (instancetype)copyWithZone:(NSZone *)zone {
    AVISmartImageColor *newColor = [[[self class] allocWithZone:zone] init];
    if (newColor) {
        newColor.r = self.r;
        newColor.g = self.g;
        newColor.b = self.b;
        newColor.a = self.a;
    }
    return newColor;

}

@end
