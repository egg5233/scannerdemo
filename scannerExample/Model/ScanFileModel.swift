//
//  ScanFileModel.swift
//  scannerExample
//
//  Created by David Chen on 17/07/2018.
//  Copyright © 2018 avision. All rights reserved.
//

import Foundation

struct ScanFileModel {
    var fileName:String?
    var page:Int?
}
