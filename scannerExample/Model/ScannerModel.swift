//
//  ScannerModel.swift
//  scannerExample
//
//  Created by David Chen on 13/07/2018.
//  Copyright © 2018 avision. All rights reserved.
//

import Foundation

struct ScannerModel {
    var productid:String?
    var macaddr:String?
    var deviceName:String?
    var vendorName:String?
    var serialNo:String?
    var ipaddr:String?
    var infoLength:NSNumber?
    var capability:String?
    var pid:String?
    var vid:String?
    var scsiPort:String?
    // AV120FW is 5547, MiBox is 5549.

    var deviceType:String?
    
}
