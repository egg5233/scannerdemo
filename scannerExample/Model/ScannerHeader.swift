//
//  ScannerHeader.swift
//  scannerExample
//
//  Created by David Chen on 17/07/2018.
//  Copyright © 2018 avision. All rights reserved.
//

import Foundation

struct ScannerHeader {
    var dataLength:Int?
    var dataOffset:Int?
    var version:Int?
    var scannerStatus:Data?
    var jobStatus:Int?
    var pageNo:Int?
    var sourceType:Int?
    var message:String?
    var headerLength:Int?
    
    mutating func transform(data:Data){
        dataLength = Int(CFSwapInt32LittleToHost(data.subdata(in: Range(0...3)).uint32))
        dataOffset = Int(CFSwapInt32LittleToHost(data.subdata(in: Range(4...7)).uint32))
        version = Int(CFSwapInt32LittleToHost(data.subdata(in: Range(8...9)).uint32))
        scannerStatus = data.subdata(in: Range(10...13))
        jobStatus = Int([UInt8](data.subdata(in: Range(14...14)))[0])
        pageNo = Int(CFSwapInt32LittleToHost(data.subdata(in: Range(15...18)).uint32))
        sourceType = Int([UInt8](data)[19])
        message = String.init(data: data.subdata(in: Range(20...147)), encoding: .utf8)
        headerLength = 148
    }
    
    func isErrorHappener() -> Bool {
        switch CFSwapInt32LittleToHost((scannerStatus?.subdata(in: Range(0...1)).uint32)!) {
        case SCANNER_STATUS_IDLE:
            return false
        case SCANNER_STATUS_SCANNING:
            return false
        case SCANNER_STATUS_ADFPAPEREND:
            return false
        default:
            return false
        }
    }
}
