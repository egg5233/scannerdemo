//
//  S2AImageViewController.swift
//  Scan2App
//
//  Created by David Chen on 07/11/2017.
//  Copyright © 2017 David Chen. All rights reserved.
//

import UIKit

class S2AImageViewController: UIViewController {

    var rotatedAngle:Double=0
    
    @IBAction func rotateButtonPressed(_ sender: Any) {

        if #available(iOS 10.0, *) {
            rotatedAngle+=90
            DispatchQueue.global().async {
                DispatchQueue.main.async {
                    let newImage = self.image.rotated(by: Measurement(value: self.rotatedAngle, unit: .degrees))
                    self.imageViewWidthConstrait.constant=(newImage?.size.width)!
                    self.imageViewHeightConstraint.constant=(newImage?.size.height)!
                    self.scrollView.contentSize=(newImage?.size)!
                    self.imageView.image=newImage
                }
            }

        } else {
            // Fallback on earlier versions
        }
    }
    var image:UIImage!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var imageViewWidthConstrait: NSLayoutConstraint!
    @IBOutlet weak var imageViewHeightConstraint: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        imageViewWidthConstrait.constant=self.image.size.width
        imageViewHeightConstraint.constant=self.image.size.height
        scrollView.contentSize=self.image.size
        DispatchQueue.main.async {
            self.imageView.image=self.image
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension S2AImageViewController:UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
}

extension UIImage {
    struct RotationOptions: OptionSet {
        let rawValue: Int
        
        static let flipOnVerticalAxis = RotationOptions(rawValue: 1)
        static let flipOnHorizontalAxis = RotationOptions(rawValue: 2)
    }
    
    @available(iOS 10.0, *)
    func rotated(by rotationAngle: Measurement<UnitAngle>, options: RotationOptions = []) -> UIImage? {
        guard let cgImage = self.cgImage else { return nil }
        
        let rotationInRadians = CGFloat(rotationAngle.converted(to: .radians).value)
        let transform = CGAffineTransform(rotationAngle: rotationInRadians)
        var rect = CGRect(origin: .zero, size: self.size).applying(transform)
        rect.origin = .zero
        
        let renderer = UIGraphicsImageRenderer(size: rect.size)
        return renderer.image { renderContext in
            renderContext.cgContext.translateBy(x: rect.midX, y: rect.midY)
            renderContext.cgContext.rotate(by: rotationInRadians)
            
            let x = options.contains(.flipOnVerticalAxis) ? -1.0 : 1.0
            let y = options.contains(.flipOnHorizontalAxis) ? 1.0 : -1.0
            renderContext.cgContext.scaleBy(x: CGFloat(x), y: CGFloat(y))
            
            let drawRect = CGRect(origin: CGPoint(x: -self.size.width/2, y: -self.size.height/2), size: self.size)
            renderContext.cgContext.draw(cgImage, in: drawRect)
        }
    }
}
