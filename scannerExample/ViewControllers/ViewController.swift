//
//  ViewController.swift
//  scannerExample
//
//  Created by David Chen on 13/07/2018.
//  Copyright © 2018 avision. All rights reserved.
//

import UIKit
import SwiftProgressHUD

class ViewController: UIViewController {

    @IBOutlet weak var manualIPAddressTextField: UITextField!
    @IBOutlet weak var detectButton: UIButton!
    @IBOutlet weak var scanButton: UIButton!
    @IBOutlet weak var detectedIPLabel: UILabel!

    @IBAction func detectButtonPressed(_ sender: Any) {
        SwiftProgressHUD.showWait()
        SearchScannerHelper.shared().searchScanner()
    }
    
    @IBAction func scanButtonPressed(_ sender: Any) {
        SwiftProgressHUD.showWait()
        if (manualIPAddressTextField.text?.count)! > 0 {
            ScanToAppHelper.shared().scan(at:manualIPAddressTextField.text! , with:selectedParams)
            return
        }
        
        guard scannerList.count > 0 else {
            print("no scanner found")
            return
        }
        let scanner  = scannerList.first!
        ScanToAppHelper.shared().scan(at:scanner.ipaddr! , with:selectedParams)
    }
    
    private var selectedParams:[String:String] =  [String:String]()
    private var scannerList: [ScannerModel] = [ScannerModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SwiftProgressHUD.showWait()
        // Do any additional setup after loading the view, typically from a nib.
        selectedParams[PARAMS_COLOR] = "Color"
        selectedParams[PARAMS_FORMAT] = "JPEG"
        selectedParams[PARAMS_SIZE] = "8500x14000"
        selectedParams[PARAMS_DPI] = "300"
        selectedParams[PARAMS_DUPLEX] = "No"
        selectedParams[PARAMS_OVERSCAN] = "UP_DOWN"
        SearchScannerHelper.shared().delegate = self
        ScanToAppHelper.shared().delegate = self
        SearchScannerHelper.shared().searchScanner()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension ViewController:SearchScannerDelegate {
    
    func didTimeout() {
        SwiftProgressHUD.hideAllHUD()
    }
    
    func broadcastFinished(scannerList: [ScannerModel]?) {
        SwiftProgressHUD.hideAllHUD()
        if scannerList != nil {
            self.scannerList = scannerList!
            self.scanButton.isEnabled=true
            self.scanButton.backgroundColor=UIColor.init(red: 62/255, green: 123/255, blue: 233/255, alpha: 1)
            let scanner  = scannerList?.first!
            self.detectedIPLabel.text = "Found printer at:" + (scanner?.ipaddr)!
        }
    }
}

extension ViewController:ScanToAppHelperProtocol {

    func fileStart(fileSize: NSNumber) {
        
    }
    
    func fileEnd(filePath: String, page: Int) {
        SwiftProgressHUD.hideAllHUD()
        do {
            let imageData = try Data(contentsOf: URL.init(fileURLWithPath: filePath))
            let vc = storyboard?.instantiateViewController(withIdentifier: "S2AImageViewController") as! S2AImageViewController
            vc.image = UIImage(data: imageData)
            DispatchQueue.main.async {
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } catch {
            print("Error loading image : \(error)")
        }
    }
}

extension ViewController:UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == manualIPAddressTextField {
            if (textField.text?.count)! > 0 {
                self.scanButton.isEnabled=true
                self.scanButton.backgroundColor=UIColor.init(red: 62/255, green: 123/255, blue: 233/255, alpha: 1)
            }
        }
    }
}
