#ifndef _KERNEL_H
#define _KERNEL_H

#if (defined(_WINDOWS) || defined(_WIN32) || defined(_WIN64))
#include <windows.h>
#endif //(defined(_WINDOWS) || defined(_WIN32) || defined(_WIN64))

#include "Errors.h"

#pragma pack(1)

/*#ifdef _MAC
	#define CALLBACK    PASCAL
	#define WINAPI      CDECL
	#define APIENTRY    WINAPI
 #ifdef _68K_
	#define PASCAL      __pascal
 #else
	#define PASCAL
 #endif
#elif (_MSC_VER >= 800) || defined(_STDCALL_SUPPORTED)
	#define CALLBACK    __stdcall
	#define WINAPI      __stdcall
	#define APIENTRY    WINAPI
	#define PASCAL      __stdcall
#else
	#define CALLBACK
	#define WINAPI
	#define APIENTRY    WINAPI
	#define APIPRIVATE
	#define PASCAL      pascal
#endif*/
#if (defined(_WINDOWS) || defined(_WIN32) || defined(_WIN64))
	#ifdef WIN32
        #define TW_HUGE
    #else //WIN32
        #define TW_HUGE    huge
	#endif //WIN32
#else //(defined(_WINDOWS) || defined(_WIN32) || defined(_WIN64))
	#define BYTE 	unsigned char
	#define WORD	unsigned short
#ifdef __LP64__
	#define DWORD       unsigned int
#else
	#define DWORD		unsigned long
#endif
  #if !(defined(_VXWORKS) || defined(VXWORKS))
	#define BOOL		int
  #endif //!(defined(_VXWORKS) || defined(VXWORKS))
	
	#define LPSTR	char*
	#define FAR
	#define WINAPI
#endif //(defined(_WINDOWS) || defined(_WIN32) || defined(_WIN64))


#define AUTOCOLORDETECT				0x8000
#define COMPACTMODE					0x2000
#define BWIMAGE						0x0001
#define GRAYIMAGE					0x0002
#define COLORIMAGE					0x0004	//RGB Image
#define RGBAIMAGE					0x0008	//RGBA Image, BitsPerPixel=32
#define ARGBIMAGE					0x0010	//ARGB Image, BitsPerPixel=32
#define JPEGACCESS					0x0080
#define MULTIPLECROP				0x80
#define SINGLECROPOFDUPLEX			0x40
#define SINGLEFULLCROP				0x20
#define NOCARRIERSHEET				0x10
#define USEA4SHEET					0x08
#define CROPWITHINIMAGESIZE			0x02
#define AUTOCROP					0x01
#define FLATBED						0
#define SIMPLEX						1
#define DUPLEX						2
#define SIMPLEXREAR					3
#define MERGE2SIDE					0x01
#define MERGEROTATE90				0x02
#define MERGEROTATE270				0x04
#define MERGEROTATE180				0x06
#define MERGEDISABLESIDEDETECT		0x08
#define MERGEINVERTED				0x10	//Only work with MERGEDISABLESIDEDETECT
#define MERGEROTATEFIRST			0x20	//Disable when MERGEVERTICAL
#define MERGEVERTICALNOFLIP			0x40	//Auto enable when MERGEVERTICAL
#define MERGEVERTICAL				0x80
#define BGPDOCTYPETEXT				0x10
#define BGPDOCTYPEPHOTO				0x20
#define BGPDOCTYPEMIXED				(BGPDOCTYPETEXT | BGPDOCTYPEPHOTO)
#define BGPMODEREMOVAL				0x40
#define BGPMODEREMOVALALL			0xC0
#define BGPMODEPADWITHBG			0xC2
#define BGPMODEREMOVEHATCH			0x01
#define FLR_HORVER_NONE             0
#define FLR_HORVER_AUTO             1
#define FLR_HORVER_HORIZONTAL       2
#define FLR_HORVER_VERTICAL         3
#define FLR_LINE_LENGTH_DEFAULT     -1

typedef struct {
	int		X;							//start X coordinate
	int		Y;							//start Y coordinate
	int		Pixel;						//width with pixel
	int		Line;						//high with pixel
	DWORD	Type;						//defination of barcode type
	union {
		DWORD	Param;					//reserved: only supported 0, bit 0: recognize define area, 1:recognize out of defination area
		char	*Info;
	}Extend;
}BARCODEINFO,  FAR *pLBARCODEINFO;

typedef struct {
	DWORD	Size;						//size of the structure
	char	Model[24];					//model name

	struct {
		BYTE	ScanType;				//0:flatbed	    1:adf front	    2:duplex	3:adf rear
		BYTE	ScanMode;				//0x01:b/w, 0x02:gray, 0x04:color, 
										//0x08:RGBA, 0x10:ARGB, 
										//0x80:JPEGACCESS - Internal using, only for JpegCrop().
		BYTE	BitsPerPixel;			//bits per pixel
		BYTE	SamplePerPixel;			//sample per pixel
		WORD	x;						//left x axis pixel
		WORD	y;						//top y axis pixel
		DWORD	Pixel;					//image width with pixel
		DWORD	Line;					//image length with pixel
		WORD	CurPages;				//current scan page number
		WORD	ScanTypeMaxWidth;		//max width of the scan type, base on the X resolution(i.e. the variable ��XRes�� defined next)
		WORD	XRes;					//scan resolution
		WORD	YRes;					//scan resolution
		DWORD	BytesPerLine;			//bytes per line
		BYTE	ColorFilter;			//color drop out filter
		BYTE	BackgroundColor;		//the scanner background color of the scan type
		WORD	EOPGap;					//internel used
		BYTE	RefSkewAngle[2];		//skew angle detect by O/M
		BYTE	Reserved1[2];
	}InImage;

	struct {
		WORD	OutMode;				//bit control
										//bit 15: auto color detection
										//bit 14: ignore background color(only work for bit 15 enabled)
										//bit 13: COMPACTMODE (0x2000) - Internal using, only for JpegCrop().
										//bit 7: JPEGACCESS - Internal using, only for JpegCrop().
										//bit 4: ARGB
										//bit 3: RGBA
										//bit 2: color
										//bit 1: gray
										//bit 0: b/w
		BYTE	ACThreshold ;			//auto color detection threshold, default: 37
		WORD	Option;					//bit 15: Auto Resolution (0:by length�B1:by photo or non photo(document))
		struct {
			BYTE	Mode;				//0:lineart		1:dynamic	2~9:halftone	10:dynamic (advance mode)
			BYTE	AdvDrop;			//0:disable		1:enable
			BYTE	ColorFilter;	    //color drop out filter, 0:master 1:R   2:G	    3:B    4:custom  
			BYTE	DropThreshold;	    //advance color filter threshold, range:0~100
			WORD	BytesPerLine;	    //bytes per line
			BYTE	BackgroundColor;    //0:black		255:white
			BYTE	BWThreshold;	    //black and white threshold, only work for Mode=0 (lineart)
			BYTE	DyThreshold;	    //reference boundary of dynamic threshold, 0:default	range:0~100
			BYTE	HalftoneSize[2];    //byte 0:x coordination, byte 1:y coordination
			BYTE	BitsGTable;			//bits of the gamma table
			void	*HalftoneTable;	    //
			void	*TableM;			//gamma table of the master channel
			BYTE	Compression;	    //compression mode, 0:none	1:G4	
			BYTE	RDropValue;			//custom color drop value for R channel, only work for ColorFilter=4
			BYTE    GDropValue;			//custom color drop value for G channel, only work for ColorFilter=4
			BYTE	BDropValue;			//custom color drop value for B channel, only work for ColorFilter=4
			BYTE    DropTolerance;	    //custom color drop tolerance, only work for ColorFilter=4, range:1~100, default:10
			BYTE	Reserved1[3];
		}BW;

		struct {
			BYTE	Mode;				//0:document	1:photo		2:normal  mode
			BYTE	AdvDrop;			//0:disable		1:enable
			BYTE	ColorFilter;	    //color drop out filter, 0:master	1:R	2:G	3:B
			BYTE	DropThreshold;	    //advance color filter threshold
			BYTE	BackgroundLevel;    //white reference for background value, 0:default
			BYTE	BackgroundColor;    //the background color 0~255
			BYTE	Compression;	    //compression mode, 0:none	2:JPEG
			BYTE	BitsGTable;			//bits of the gamma table
			void	*TableM;			//gamma table of the master channel
			BYTE	JPEGQuality;		//Bit position: High->(7,6,5,4,3,2,1,0)->Low
										//0~3 bit: image quality of JPEG compression, range:0~15, 0:high compression	15:low compression
										//4~6 bit: sampling rate. 0: default 422; 1: 444(high quality); 2: 422; 3: 411(low quality).
			BYTE	RDropValue;			//custom color drop value for R channel, only work on ColorFilter=4
			BYTE    GDropValue;			//custom color drop value for G channel, only work on ColorFilter=4
			BYTE	BDropValue;			//custom color drop value for B channel, only work on ColorFilter=4
			BYTE    DropTolerance;	    //custom color drop tolerance, only work for ColorFilter=4, range:1~100, default:10
			BYTE	Reserved1[11];
		}Gray;

		struct {
			BYTE	Mode;				//0:RGB piexl pack	1:BGR pixel pack	2:planner channel
			BYTE	IccProfile;			//0:disable		1:enable
			BYTE	BackgroundColorR;   //R channel value for background color 0~255
			BYTE	BackgroundColorG;   //G channel value for background color 0~255
			BYTE	BackgroundColorB;   //B channel value for background color 0~255
			BYTE	Reserved[2];
			BYTE	BitsGTable;			//bits of the gamma table
			void	*TableM;			//gamma table of the master channel
			void	*TableR;			//gamma table of the R channel
			void	*TableG;			//gamma table of the G channel
			void	*TableB;			//gamma table of the B channel
			BYTE	Compression;	    //compression mode, 0:none	2:JPEG
			BYTE	JPEGQuality;		//Bit position: High->(7,6,5,4,3,2,1,0)->Low
										//0~3 bit: image quality of JPEG compression, range:0~15, 0:high compression	15:low compression
										//4~6 bit: sampling rate. 0: default 422; 1: 444(high quality); 2: 422; 3: 411(low quality).
			BYTE	Reserved1[2];		
		}Color;

		BYTE	Merge2Side;				//Bit position: High->(7,6,5,4,3,2,1,0)->Low
										//Bit 0  : 0-disable / 1-enable
										//         -------   -------     --------------
										//         |     |   |     |     |     ||     |
										//         | --- |   | /-  |     | --- || /-  |
										//         |  |  | + |  |  |  =  |  |  ||  |  |
										//         |     |   |     |     |     ||     |
										//         -------   -------     --------------
										//           Front      Rear          Output
										//Bit 1~2: (Bit 2, Bit 1)-orientation angle of "rear" side
										//         (    0,     0)-0�X, default
										//         (    0,     1)-CW90�X
										//         -------   -------     -------|--------|
										//         |     |   |     |     |     ||    \   |
										//         | --- |   | /-  |     | --- ||   -|   |
										//         |  |  | + |  |  |  =  |  |  ||--------|
										//         |     |   |     |     |     |
										//         -------   -------     -------
										//           Front      Rear          Output
										//         (    1,     0)-CCW90�X
										//         -------   -------     -------|--------|
										//         |     |   |     |     |     ||   |-   |
										//         | --- |   | /-  |     | --- ||   \    |
										//         |  |  | + |  |  |  =  |  |  ||--------|
										//         |     |   |     |     |     |
										//         -------   -------     -------
										//           Front      Rear          Output
										//         (    1,     1)-180�X
										//         -------   -------     --------------
										//         |     |   |     |     |     ||     |
										//         | --- |   | /-  |     | --- ||  |  |
										//         |  |  | + |  |  |  =  |  |  ||  -/ |
										//         |     |   |     |     |     ||     |
										//         -------   -------     --------------
										//           Front      Rear          Output
										//Bit 3~4: (Bit 4, Bit 3)-Enable/disable front/rear side recognition.
										//         (    X,     0)-Default: enable auto side recognition.
										//         (    0,     1)-Non-inverted.
										//         -------   -------     --------------
										//         |     |   |     |     |     ||     |
										//         | --- |   | /-  |     | --- || /-  |
										//         |  |  | + |  |  |  =  |  |  ||  |  |
										//         |     |   |     |     |     ||     |
										//         -------   -------     --------------
										//           Front      Rear          Output
										//         (    1,     1)-Inverted.
										//         -------   -------     --------------
										//         |     |   |     |     |     ||     |
										//         | --- |   | /-  |     | /-  || --- |
										//         |  |  | + |  |  |  =  |  |  ||  |  |
										//         |     |   |     |     |     ||     |
										//         -------   -------     --------------
										//           Front      Rear          Output
										//Bit 5  : 0-rotate merged output image (merge first). 1-Rotate both side image separately (rotate first).
										//         Bit 5 = 1 would only run as Bit 7 = 0.
										//Bit 6  : 0-normal merge / 1-don't flip rear side when running vertical merging
										//         Bit 6 = 1 would only run as Bit 7 = 1.
										//         -------   -------     -------
										//         |     |   |     |     |     |
										//         | --- |   | /-  |     | --- |
										//         |  |  | + |  |  |  =  |  |  |
										//         |     |   |     |     |     |
										//         -------   -------     =======
										//                               |     |
										//                               | /-  |
										//                               |  |  |
										//                               |     |
										//                               -------
										//           Front      Rear      Output
										//Bit 7  : 0-merge by horizontal / 1-merge by vertical
										//         -------   -------     -------
										//         |     |   |     |     |     |
										//         | --- |   | /-  |     | --- |
										//         |  |  | + |  |  |  =  |  |  |
										//         |     |   |     |     |     |
										//         -------   -------     =======
										//                               |     |
										//                               |  |  |
										//                               |  -/ |
										//                               |     |
										//                               -------
										//           Front      Rear      Output
		BYTE	Despeckle;				//0:disable		1:enable
		BYTE	AutoCrop;				//Bit position: High->(7,6,5,4,3,2,1,0)->Low
										//Bit 0: 0-disable / 1-enable
										//Bit 1: 1-Crop document within image size / 0-disable.
										//Bit 3: 1-Use A4 size sheet. / 0-Use A5 size sheet.
										//Bit 4: Not detect the carrier sheet
										//Bit 5: Multiple crop deskew for single full crop
										//Bit 6: single crop of the duplex mode
										//Bit 7: for multiple cropping
		BYTE	Rotate;					//Rotate the image
										//0:disable  1:deskew  2:auto orientation(rotate base on text)  3:CW90�X  4:CCW90�X  5:180�X
		DWORD	ImageWidth;				//output image width with pixel
		DWORD	ImageHeight;			//output image height with pixel
		DWORD	RefBlankSize;			//bytes, image size that's help to identify the blank page
		BYTE	BlankPageDetect;		//0:disable		1:enable
		BYTE	BlankThreshold;			//threshold of the blank page, range: 0~100
		BYTE	HoleRemoval;			//0:disable
										//1:high quality mode(support black, gray and white background color of scanner)
										//2:speed up mode(only support black and gray background color of scanner)
		BYTE	InternalUse;			//Notice! Internal use only! Don't change this variable.
		BYTE	BackgroundSmooth;		//Control Codes (defined in Kernel.h): 
										//  document type �V 
										//    Default: run with performance mode
										//    BGPDOCTYPETEXT: text
										//    BGPDOCTYPEPHOTO: photo
										//    BGPDOCTYPEMIXED: mixed
										//  mode �V 
										//    0: default(smooth)
										//    BGPMODEREMOVAL: remove the main color
										//    BGPMODEREMOVALALL: remove all background
										//    BGPMODEPADWITHBG: remove all background with the main color.
										//  quality - 
										//    BGPMODEREMOVEHATCH: remove hatch patterns.
										//
										//Example 1: 
										//type=BGPDOCTYPETEXT, mode=0 (smooth)
										//NIDATA.OutImage.BackgroundSmooth = BGPDOCTYPETEXT;
										//
										//Example 2: 
										//type=BGPDOCTYPEMIXED, mode=BGPMODEREMOVAL
										//NIDATA.OutImage.BackgroundSmooth = BGPDOCTYPEMIXED | BGPMODEREMOVAL;
		BYTE	Filter;					//filter function	0:disable	1:blurring	2:sharpening	3:unsharp mask
		union {
			BYTE	BackgroundSensitivity; //Background smooth level, 
										   //range:0~4, 
										   //0: Default settings.
										   //1: smooth/remove more background.
										   //4: smooth/remove less background.(keep more details)
			BYTE	UnsharpMskSensitivity;
			BYTE	FilterMaskSize;
		}Mask;							//0~15
		BYTE	EdgeCleanup;			//0:disable 1:Fix mode 2:Auto(clean dark edge)
		struct {
			WORD	Left;				//number of pixel
			WORD	Top;				//number of pixel
			WORD	Right;				//number of pixel
			WORD	Bottom;				//number of pixel
		}Edge;
		struct {
			short	Left;				//number of pixel
			short	Top;				//number of pixel
			short	Right;				//number of pixel
			short	Bottom;				//number of pixel
		}AdjCrop;
		struct {
			char	isZeroBlack;		//"isZeroBlack == any non-zero number" means 0 is black; otherwise, 0 is white
			BYTE	ratio;				//the size of a rectangle used to determine if there are noises
			BYTE	dotsNumber;			//the threshold to determine if points are noises or not
		}Noise;
		struct
		{
			BYTE AOMode;				//0: Default(Full image + Fast mode)
										//1: Full image + Accurate mode
										//2: Assigned area + Fast mode
										//3: Assigned area + Accurate mode
										//4: Avoid area + Fast mode
										//5: Avoid area + Accurate mode
										//6: Complexity mode
			WORD AreaLeft;				//Assigned/Avoid area to do AO
			WORD AreaTop;				//Assigned/Avoid area to do AO
			WORD AreaRight;				//Assigned/Avoid area to do AO
			WORD AreaBottom;			//Assigned/Avoid area to do AO
		}AO;

		struct
		{
			WORD JPEGDecodePosY;
			WORD JPEGDecodeLines;		//Decoding by strips, must be 16 of multiple.
										//StripLine: 0 - decodethe whole image.
		} Jpeg;

		struct
		{
			BYTE byteLineTypeBeRemoved;
			short sLinePixelLengthIn300Dpi;
		} FormLineRemoval;

		BYTE	Reserved[29];
	}OutImage;

} NIDATA,  FAR *pLNIDATA;
 
//export function
typedef long (WINAPI *InitializeLibProc)(LPSTR);
typedef long (WINAPI *InitializeLibExProc)(LPSTR company, DWORD *pdwStatus);
typedef long (WINAPI *TerminateLibProc)(void);
typedef long (WINAPI *GrayQualityProc)(void *pSrc, void **pDes, pLNIDATA pNi, void *pReserved, long *pStatus);
typedef long (WINAPI *DeskewCropProc)(void *pSrc, void **pDes, pLNIDATA pNi, void *pReserved, long *pStatus);
typedef long (WINAPI *AdvColorDropProc)(void *pSrc, void **pDes, pLNIDATA pNi, void *pReserved, long *pStatus);
typedef long (WINAPI *EdgeCleanupProc)(void *pSrc, void **pDes, pLNIDATA pNi, void *pReserved, long *pStatus);
typedef long (WINAPI *AutoColorDetectionProc)(void *pSrc, void **pDes, pLNIDATA pNi, void *pReserved, long *pStatus);
typedef long (WINAPI *BlankPageDetectionProc)(void *pSrc, void **pDes, pLNIDATA pNi, void *pReserved, long *pStatus);
typedef long (WINAPI *AutoOrientationProc)(void *pSrc, void **pDes, pLNIDATA pNi, void *pReserved, long *pStatus);
typedef long (WINAPI *PunchHoleRemovalProc)(void *pSrc, void **pDes, pLNIDATA pNi, void *pReserved, long *pStatus);
typedef long (WINAPI *DoDespeckleProc)(void *pSrc, void **pDes, pLNIDATA pNi, void *pReserved, long *pStatus);
typedef long (WINAPI *BackgroundProcessProc)(void *pSrc, void **pDes, pLNIDATA pNi, void *pReserved, long *pStatus);
typedef long (WINAPI *SwitchBackgroundProc)(void *pSrc, void **pDes, pLNIDATA pNi, void *pReserved, long *pStatus);
typedef long (WINAPI *Merge2SideProc)(void *pSrc, void **pDes, pLNIDATA pNi, void *pSrc2, long *pStatus);
typedef long (WINAPI *DoFilterProc)(void *pSrc, void **pDes, pLNIDATA pNi, void *pReserved, long *pStatus);
typedef long (WINAPI *lThresholdProc)(void *pSrc, void **pDes, pLNIDATA pNi, void *pReserved, long *pStatus);
typedef long (WINAPI *MultiStreamProc)(void *pSrc, void **pDes, pLNIDATA pNi, void *pReserved, long *pStatus);
typedef long (WINAPI *GetMultipleImageProc)(void *pReserved0, void **pDes, pLNIDATA pNi, void *pReserved, long *pStatus);
typedef long (WINAPI *DeskewCropContProc)(void *pSrc, void **pDes, pLNIDATA pNi, void *pReserved, long *pStatus, long *plSrcRequestLines);
typedef long (WINAPI *GetDeskewCropContImageProc)(void *pSrc, void **pDes, pLNIDATA pNi, void *pSrcRequest, long *pStatus, long lSrcRequestLines);
typedef long (WINAPI *JpegCropProc)(void *pSrc, void **pDes, pLNIDATA pNi, 
									void *pulDesJFIFSize, long *pStatus, 
									unsigned long ulSrcJFIFSize, void *plDesStripHeight);
typedef long (WINAPI *JpegCropContProc)(void *pSrc, void **pDes, pLNIDATA pNi, 
										void *pulDesJFIFSize, long *pStatus, 
										unsigned long ulSrcJFIFSize, 
										void *pSrcRequest, 
										unsigned long ulSrcRequestJFIFSize, 
										long lSrcRequestLines, 
										void *plDesStripHeight);
typedef long (WINAPI *JpegEncoderProc)(void *pSrc, void **pDes, pLNIDATA pNi, void *pulJFIFSize, long *pStatus);
typedef long (WINAPI *JpegDecoderProc)(void *pSrc, void **pDes, pLNIDATA pNi, void *pulJpegSize, long *pStatus);
typedef long (WINAPI *PatchCodeProc)(void *pSrc, void **pDes, pLNIDATA pNi, void *plCreateType, long *pStatus);
typedef long (WINAPI *SetDebugModeProc)(DWORD debugmode, LPSTR debugpath);
typedef long (WINAPI *FreeMemoryProc)(void *pointer);
typedef long (WINAPI *IdentifyProductSettingProc)(LPSTR id, void *datFile, long *pStatus);
typedef long (WINAPI *AdvColorDrop2Proc)(void *pSrc, void **pDes, pLNIDATA pNi, void *pCustomColor, long *pStatus);
typedef long (WINAPI *AdvColorKeepProc)(void *pSrc, void **pDes, pLNIDATA pNi, void *pReserved, long *pStatus);
typedef long (WINAPI *AutoResolutionProc)(void *pSrc, void **pDes, pLNIDATA pNi, void *pReserved, long *pStatus);
typedef long (WINAPI *BarcodeDetectionProc)(void *pSrc, void **pDes, pLNIDATA pNi, void *pReserved, long *pStatus);
typedef long (WINAPI *BookScanCorrectionProc)(void *pSrc, void **pDes, pLNIDATA pNi, void *pReserved, long *pStatus);
typedef long (WINAPI *FormLinesRemovalProc)(void *pSrc, void **pDes, pLNIDATA pNi, void *pReserved, long *pStatus);


#pragma pack()

#endif //_KERNEL_H

