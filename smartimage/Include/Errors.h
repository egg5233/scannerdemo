#ifndef _ERRORS_H
#define _ERRORS_H

//error code
#define Success					0				//Success
#define ERR_UNKNOW				-4000			//unknow error
#define ERR_LOWMEMORY			-4001			//Not enough memory to perform operation
#define ERR_NOTSUPPORT			-4002			//not support
#define ERR_BADVALUE			-4003			//Data parameter out of range
#define ERR_EXCEPTION			-4004			//exception error of the command
#define ERR_FAILURE				-4005			//failure to do xxxx
#define ERR_OUTOFDATENUMBER		-4006			//serial number is out of date
#define ERR_ILLEGALINPUTBUFFER	-4007			//illegal input buffer

//detail status
#define Success					0				//success
#define SS_MONODETECT			-1001			//mono image detected
#define SS_COLORDETECT			-1002			//color image detected
#define SS_BLANKPAGE			-1003			//blank page detected
#define SS_COLORONLY			-1004			//only work on color image
#define SS_GRAYONLY				-1005			//only work on gray image
#define SS_BWUNSUPPORTED		-1006			//only work on gray or color image
#define SS_ILLEGALDATA			-1007			//illgal data
#define SS_BITSUNSUPPORTED		-1008			//bits per pixel unsupported
#define SS_DATAMISMATCH			-1009			//there is no request data OR request information mismatch
#define SS_ANGLEFAILURE			-1010			//angle detect failure
#define SS_ACCESSVIOLATION		-1011			//access violation memory
#define SS_LOWMEMORY			-1012			//Not enough memory to perform operation or allocate memory failure
#define SS_ILLEGALPOINTER		-1013			//the pointer is null or illegal
#define SS_FUNCTIONUNSPPORTED	-1014			//this function unsupport
#define SS_FAILURE				-1015			//failure to do xxxx
#define SS_FAILTOCROP         -1016       //fail to crop the image
#define SS_FAILTODESKEW       -1017       //fail to deskew the image
#define SS_ILLEGALINPUTBUFFER	-1018			//illegal input buffer
#define SS_CW0					-1019			// rotate 0 degree in clockwise
#define SS_CW90					-1020			// rotate 90 degree in clockwise
#define SS_CW180				-1021			// rotate 180 degree in clockwise
#define SS_CW270				-1022			// rotate 270 degree in clockwise
#define SS_TOOSMALLINPUTBUFFER	-1023			//The buffer user provided is too small.
#define SS_UNPROCESSED			-1024			//Return the original image.
#define SS_PATCH01				-1025			//Patch code 1
#define SS_PATCH02				-1026			//Patch code 2
#define SS_PATCH03				-1027			//Patch code 3
#define SS_PATCH04				-1028			//Patch code 4
#define SS_PATCH05				-1029			//Patch code 5
#define SS_PATCH06				-1030			//Patch code 6
#define SS_PATCH07				-1031			//Patch code 7
#define SS_PATCH08				-1032			//Patch code 8
#define SS_PATCH09				-1033			//Patch code 9
#define SS_PATCH10				-1034			//Patch code 10
#define SS_JPEGLOWMEMORY		-1035			//Not enough memory for processing
#define SS_MULTIOUTPUTS			-1036			//Multiple outputs
#define SS_NOTSUPPORTLONGIMAGE	-1037			//Not support long image

#endif
