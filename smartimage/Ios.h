//  Ios.h

#import <Foundation/Foundation.h>
#import "Include/Kernel.h"
#import "Include/Errors.h"

//
//  Ios.h
//  Ios
//
//  Created by arlen_hsu on 15/5/21.
//  Copyright (c) 2015年 arlen_hsu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SmartImageSL : NSObject

//export function

-(long) InitializeLibProc:(LPSTR) company;
-(long) TerminateLibProc;
-(long) GrayQualityProc:(void *) pSrc and: (void **) pDes and: (pLNIDATA) pNi and: (void *) pReserved and: (long *) pStatus;
-(long) DeskewCropProc:(void *) pSrc and: (void **) pDes and: (pLNIDATA) pNi and: (void *) pReserved and: (long *) pStatus;
-(long) AdvColorDropProc:(void *) pSrc and: (void **) pDes and: (pLNIDATA) pNi and: (void *) pReserved and: (long *) pStatus;
-(long) EdgeCleanupProc:(void *) pSrc and: (void **) pDes and: (pLNIDATA) pNi and: (void *) pReserved and: (long *) pStatus;
-(long) AutoColorDetectionProc:(void *) pSrc and: (void **) pDes and: (pLNIDATA) pNi and: (void *) pReserved and: (long *) pStatus;
-(long) BlankPageDetectionProc:(void *) pSrc and: (void **) pDes and: (pLNIDATA) pNi and: (void *) pReserved and: (long *) pStatus;
-(long) AutoOrientationProc:(void *) pSrc and: (void **) pDes and: (pLNIDATA) pNi and: (void *) pReserved and: (long *) pStatus;
-(long) PunchHoleRemovalProc:(void *) pSrc and: (void **) pDes and: (pLNIDATA) pNi and: (void *) pReserved and: (long *) pStatus;
-(long) DoDespeckleProc:(void *) pSrc and: (void **) pDes and: (pLNIDATA) pNi and: (void *) pReserved and: (long *) pStatus;
-(long) BackgroundProcessProc:(void *) pSrc and: (void **) pDes and: (pLNIDATA) pNi and: (void *) pReserved and: (long *) pStatus;
-(long) SwitchBackgroundProc:(void *) pSrc and: (void **) pDes and: (pLNIDATA) pNi and: (void *) pReserved and: (long *) pStatus;
-(long) Merge2SideProc:(void *) pSrc and: (void **) pDes and: (pLNIDATA) pNi and: (void *) pSrc2 and: (long *) pStatus;
-(long) DoFilterProc:(void *) pSrc and: (void **) pDes and: (pLNIDATA) pNi and: (void *) pReserved and: (long *) pStatus;
-(long) lThresholdProc:(void *) pSrc and: (void **) pDes and: (pLNIDATA) pNi and: (void *) pReserved and: (long *) pStatus;
-(long) MultiStreamProc:(void *) pSrc and: (void **) pDes and: (pLNIDATA) pNi and: (void *) pReserved and: (long *) pStatus;
-(long) GetMultipleImageProc:(void *) pReserved0 and: (void **) pDes and: (pLNIDATA) pNi and: (void *) pReserved and: (long *) pStatus;
-(long) DeskewCropContProc:(void *) pSrc and: (void **) pDes and: (pLNIDATA) pNi and: (void *) pReserved and: (long *) pStatus and: (long *) plSrcRequestLines;
-(long) GetDeskewCropContImageProc:(void *) pSrc and: (void **) pDes and: (pLNIDATA) pNi and: (void *) pSrcRequest and: (long *) pStatus and: (long) lSrcRequestLines;
-(long) JpegCropProc:(void *) pSrc and: (void **) pDes and: (pLNIDATA) pNi and:
(void *) pulDesJFIFSize and: (long *) pStatus and:
(unsigned long) ulSrcJFIFSize and: (void *) plDesStripHeight;
-(long) JpegCropContProc:(void *) pSrc and: (void **) pDes and: (pLNIDATA) pNi and:
(void *) pulDesJFIFSize and: (long *) pStatus and:
(unsigned long) ulSrcJFIFSize and:
(void *) pSrcRequest and:
(unsigned long) ulSrcRequestJFIFSize and:
(long) lSrcRequestLines and:
(void *) plDesStripHeight;
-(long) JpegEncoderProc:(void *) pSrc and: (void **) pDes and: (pLNIDATA) pNi and: (void *) pulJFIFSize and: (long *) pStatus;
-(long) JpegDecoderProc:(void *) pSrc and: (void **) pDes and: (pLNIDATA) pNi and: (void *) pulJpegSize and: (long *) pStatus;
-(long) PatchCodeProc:(void *) pSrc and: (void **) pDes and: (pLNIDATA) pNi and: (void *) plCreateType and: (long *) pStatus;
-(long) SetDebugModeProc:(DWORD) debugmode and: (LPSTR) debugpath;
-(long) FreeMemoryProc:(void *) pointer;
-(long) IdentifyProductSettingProc:(LPSTR) id_ios and: (void *)datFile and: (long *)pStatus;
-(long) AdvColorDrop2Proc:(void *) pSrc and: (void **) pDes and: (pLNIDATA) pNi and: (void *) pCustomColor and: (long *) pStatus;
-(long) AdvColorKeepProc:(void *) pSrc and: (void **) pDes and: (pLNIDATA) pNi and: (void *) pReserved and: (long *) pStatus;
-(long) AutoResolutionProc:(void *) pSrc and: (void **) pDes and: (pLNIDATA) pNi and: (void *) pReserved and: (long *) pStatus;

-(long) SetDebugMode:(DWORD) debugmode and: (LPSTR) debugpath;

@end
